gnome = import('gnome')

conf = configuration_data()
conf.set_quoted('VERSION', meson.project_version())
conf.set_quoted('GETTEXT_PACKAGE', meson.project_name())
conf.set_quoted('APPLICATION_ID', application_id)
conf.set_quoted('LOCALEDIR', get_option('prefix') / get_option('localedir'))
conf.set_quoted('PKGDATADIR', pkgdatadir)

configure_file(
    input: 'config.rs.in',
    output: 'config.rs',
    configuration: conf
)

# Copy the config.rs output to the source directory.
run_command(
  'cp',
  meson.project_build_root() / 'src' / 'config.rs',
  meson.project_source_root() / 'src' / 'config.rs',
  check: true
)

cargo_bin  = find_program('cargo')
cargo_opt  = [ '--manifest-path', meson.project_source_root() / 'Cargo.toml' ]
cargo_opt += [ '--target-dir', meson.project_build_root()  / 'src' ]
cargo_env  = [ 'CARGO_HOME=' + meson.project_build_root()  / 'cargo-home' ]

if get_option('buildtype') == 'release'
  cargo_opt += [ '--release' ]
  rust_target = 'release'
else
  rust_target = 'debug'
endif

cargo_build = custom_target(
  'cargo-build',
  build_by_default: true,
  build_always_stale: true,
  output: meson.project_name(),
  console: true,
  install: true,
  install_dir: get_option('bindir'),
  command: [
    'env', cargo_env,
    cargo_bin, 'build',
    cargo_opt, '&&', 'cp', 'src' / rust_target / meson.project_name(), '@OUTPUT@',
  ]
)

blueprints = custom_target('blueprints',
  input: [
    'application/window.blp',

    'widgets/artwork/cover.blp',

    'widgets/content.blp',
    'widgets/sidebar.blp',
    'widgets/sidebar/collection.blp',
    'widgets/sidebar/playback.blp',
    'widgets/sidebar/playback/buttons.blp',
    'widgets/sidebar/playback/labels.blp',
  ],
  output: '.',
  command: [
    find_program('blueprint-compiler'),
    'batch-compile',
    '@OUTPUT@',
    '@CURRENT_SOURCE_DIR@',
    '@INPUT@'
  ]
)


# CSS
cat = find_program('cat')
css_files = files(
  'application/application.css',
  'widgets/artwork/images.css',
  'widgets/sidebar/sidebar.css'
)

concat_parts = custom_target(
  'concat-parts',
  command: [ cat, '@INPUT@' ],
  capture: true,
  input: css_files,
  output: 'style.css',
  build_by_default: true,
)

# Resources
resources = gnome.compile_resources(
  application_id,
  '@0@.gresource.xml'.format(application_id),
  gresource_bundle: true,
  source_dir: meson.current_build_dir(),
  install: true,
  install_dir: pkgdatadir,
  dependencies: [ concat_parts, blueprints ]
)