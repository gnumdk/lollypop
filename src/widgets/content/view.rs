// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::subclass::prelude::*;
use gtk::glib;

use crate::engine::bus::{BusMessageImpl, Message};

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/widgets/view.ui")]
    pub struct View {}

    #[glib::object_subclass]
    impl ObjectSubclass for View {
        const NAME: &'static str = "View";
        type Type = super::View;
        type ParentType = adw::NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for View {}
    impl WidgetImpl for View {}
    impl NavigationPageImpl for View {}
}

glib::wrapper! {
    pub struct View(ObjectSubclass<imp::View>)
        @extends gtk::Widget;
}

impl BusMessageImpl for View {
    fn handle(&self, message: &Message) {
        let this = self.imp();
    }
}

impl View {
    pub fn new() -> View {
        View { channel }
    }
}
