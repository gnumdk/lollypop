// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::subclass::prelude::*;
use gtk::glib;

pub mod buttons;
pub mod labels;

use crate::widgets::artwork::cover::Cover;
use crate::widgets::thirdparty::amberol::waveform_view::WaveformView;
use buttons::Buttons;
use labels::Labels;

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/widgets/sidebar/playback.ui")]
    pub struct Playback {
        #[template_child]
        pub buttons: TemplateChild<Buttons>,
        #[template_child]
        pub labels: TemplateChild<Labels>,
        #[template_child]
        pub cover: TemplateChild<Cover>,
        #[template_child]
        pub wave_form_view: TemplateChild<WaveformView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Playback {
        const NAME: &'static str = "Playback";
        type Type = super::Playback;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Playback {}
    impl WidgetImpl for Playback {
        fn map(&self) {
            self.parent_map();
            self.cover.set_image(
                "/var/home/bellegarde-c/Musique/Collection/Rap Français/Svinkels/Tapis Rouge/cover.jpg",
                200
            );
        }
    }
    impl BoxImpl for Playback {}
}

glib::wrapper! {
    pub struct Playback(ObjectSubclass<imp::Playback>)
        @extends gtk::Widget;
}
