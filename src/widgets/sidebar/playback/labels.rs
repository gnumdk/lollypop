// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use adw::subclass::prelude::*;
use gtk::{self};
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Default, CompositeTemplate)]
    #[template(resource = "/widgets/sidebar/playback/labels.ui")]
    pub struct Labels {}

    #[glib::object_subclass]
    impl ObjectSubclass for Labels {
        const NAME: &'static str = "Labels";
        type Type = super::Labels;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Labels {}
    impl WidgetImpl for Labels {}
    impl BoxImpl for Labels {}
}

glib::wrapper! {
    pub struct Labels(ObjectSubclass<imp::Labels>)
        @extends gtk::Widget;
}

impl Labels {
    pub fn new() -> Self {
        glib::Object::builder().build()
    }
}
