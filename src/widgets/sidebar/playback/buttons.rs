// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::subclass::prelude::*;
use gtk::glib;

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/widgets/sidebar/playback/buttons.ui")]
    pub struct Buttons {}

    #[glib::object_subclass]
    impl ObjectSubclass for Buttons {
        const NAME: &'static str = "Buttons";
        type Type = super::Buttons;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Buttons {}
    impl WidgetImpl for Buttons {}
    impl BoxImpl for Buttons {}
}

glib::wrapper! {
    pub struct Buttons(ObjectSubclass<imp::Buttons>)
        @extends gtk::Widget;
}
