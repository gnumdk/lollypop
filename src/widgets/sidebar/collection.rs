// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::application::Application;
use crate::engine::{
    bus::{send_message, Action, BusMessageImpl, Message},
    scanner,
    scanner::Scanner,
};

use adw::subclass::prelude::*;
use gtk::{gio, glib, prelude::*};

use std::sync::Arc;
use std::thread;

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/widgets/sidebar/collection.ui")]
    pub struct Collection {
        #[template_child]
        pub progressbar: TemplateChild<gtk::ProgressBar>,

        pub scanner: Arc<Scanner>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Collection {
        const NAME: &'static str = "Collection";
        type Type = super::Collection;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Collection {}
    impl WidgetImpl for Collection {
        fn realize(&self) {
            let app = gio::Application::default()
                .expect("Failed to retrieve application singleton")
                .downcast::<Application>()
                .unwrap();
            let scanner = Arc::clone(&self.scanner);
            let sender = app.sender();
            let mut uris = Vec::new();
            self.parent_realize();

            uris.push(String::from(
                "file:///var/home/bellegarde-c/Musique/Collection/Ska",
            ));

            thread::spawn(move || scanner.scan(uris, sender));
        }
    }
    impl BoxImpl for Collection {}

    #[gtk::template_callbacks]
    impl Collection {
        #[template_callback]
        fn list_row_selected_cb(&self, row: gtk::ListBoxRow) {
            send_message(Message::Action(Action::CollectionSelected(row.index())));
        }
    }
}

glib::wrapper! {
    pub struct Collection(ObjectSubclass<imp::Collection>)
        @extends gtk::Widget;
}

impl Collection {}

impl BusMessageImpl for Collection {
    fn scanner(&self, message: &scanner::Message) {
        match message {
            scanner::Message::ScannerStarted => {
                self.imp().progressbar.show();
            }
            scanner::Message::ScannerFinished => {
                self.imp().progressbar.hide();
            }
            scanner::Message::ScannerProgress(count, total) => {
                let fraction = *count as f64 / *total as f64;
                self.imp().progressbar.set_fraction(fraction);
            }
        }
    }
}
