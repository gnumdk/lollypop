// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::Cast;

pub struct Utils {}

#[gtk::template_callbacks(functions)]
impl Utils {
    #[template_callback(function)]
    fn invert(value: bool) -> bool {
        !value
    }
    #[template_callback(function)]
    fn many(count: u32) -> bool {
        count > 1
    }
    #[template_callback(function)]
    fn not(value: bool) -> bool {
        !value
    }
    #[template_callback(function)]
    fn first_page(view: adw::NavigationView, page: Option<adw::NavigationPage>) -> bool {
        match page {
            Some(navigation_page) => match view.previous_page(&navigation_page) {
                Some(previous_page) => false,
                None => true,
            },
            None => true,
        }
    }
}
