// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::glib;

// pub mod view;

use crate::engine::bus::{Action, BusMessageImpl, Message};
use crate::models;

// use view::View;

pub enum Categories {
    Suggestions,
    Populars,
    Randoms,
    Loved,
    Recents,
    Artists,
    Albums,
    Genres,
    Playlists,
}

mod imp {
    use super::*;
    use glib::{ParamSpec, ParamSpecBoolean, Value};
    use std::sync::LazyLock;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/widgets/content.ui")]
    pub struct Content {
        #[template_child]
        pub navigation_view: TemplateChild<adw::NavigationView>,
        #[template_child]
        pub back_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub sidebar_button: TemplateChild<gtk::ToggleButton>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Content {
        const NAME: &'static str = "Content";
        type Type = super::Content;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.bind_template_callbacks();
            crate::widgets::utils::Utils::bind_template_callbacks(klass);
        }
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Content {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: LazyLock<Vec<ParamSpec>> =
                LazyLock::new(|| vec![ParamSpecBoolean::builder("show-sidebar").build()]);
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "show-sidebar" => self
                    .sidebar_button
                    .get()
                    .set_active(value.get::<bool>().unwrap()),
                _ => unimplemented!(),
            };
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "show-sidebar" => self.sidebar_button.get().is_active().to_value(),
                _ => unimplemented!(),
            }
        }
    }
    impl WidgetImpl for Content {}
    impl BoxImpl for Content {}

    #[gtk::template_callbacks]
    impl Content {
        #[template_callback]
        fn sidebar_button_toggled_cb(&self) {
            let this = self.obj();
            this.set_property("show-sidebar", self.sidebar_button.get().is_active());
        }
        #[template_callback]
        fn back_button_clicked_cb(&self) {
            let _ = self.navigation_view.pop();
        }
    }
}

glib::wrapper! {
    pub struct Content(ObjectSubclass<imp::Content>)
        @extends gtk::Widget;
}

impl BusMessageImpl for Content {
    fn action(&self, action: &Action) {
        match action {
            Action::CollectionSelected(index) => {
                self.add_category_page(index);
            }
        }
    }
}

impl Content {
    pub fn add_category_page(&self, category: &i32) {
        log::warn!("add_category_page(): {}", category);
        let mut albums = models::collection::albums::Albums::new();
        albums.set_filters(None);
    }

    pub fn add_page(&self, genres: &[i32], artists: &[i32]) {
        log::warn!("add_page()");
    }
}
