// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::glib;

pub mod collection;
pub mod playback;

use crate::engine::bus::{BusMessageImpl, Message};
use collection::Collection;
use playback::Playback;

mod imp {
    use super::*;
    use glib::{ParamSpec, ParamSpecBoolean, Value};
    use std::sync::LazyLock;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/widgets/sidebar.ui")]
    pub struct SideBar {
        #[template_child]
        pub collection: TemplateChild<Collection>,
        #[template_child]
        pub playback: TemplateChild<Playback>,
        #[template_child]
        pub sidebar_button: TemplateChild<gtk::ToggleButton>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SideBar {
        const NAME: &'static str = "SideBar";
        type Type = super::SideBar;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.bind_template_callbacks();
        }
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SideBar {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: LazyLock<Vec<ParamSpec>> = LazyLock::new(|| {
                vec![
                    ParamSpecBoolean::builder("show-sidebar").build(),
                    ParamSpecBoolean::builder("collapsed-sidebar").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "show-sidebar" => self
                    .sidebar_button
                    .get()
                    .set_active(value.get::<bool>().unwrap()),
                "collapsed-sidebar" => self
                    .sidebar_button
                    .get()
                    .set_visible(value.get::<bool>().unwrap()),
                _ => unimplemented!(),
            };
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "show-sidebar" => self.sidebar_button.get().is_active().to_value(),
                "collapsed-sidebar" => self.sidebar_button.get().get_visible().to_value(),
                _ => unimplemented!(),
            }
        }
    }
    impl WidgetImpl for SideBar {}

    #[gtk::template_callbacks]
    impl SideBar {
        #[template_callback]
        fn sidebar_button_clicked_cb(&self) {
            let this = self.obj();
            this.set_property("show-sidebar", self.sidebar_button.get().is_active());
        }
    }
}

glib::wrapper! {
    pub struct SideBar(ObjectSubclass<imp::SideBar>)
        @extends gtk::Widget;
}

impl BusMessageImpl for SideBar {
    fn handle(&self, message: &Message) {
        let this = self.imp();
        this.collection.handle(message);
    }
}
