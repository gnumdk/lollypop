// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::subclass::prelude::*;
use gtk::glib;

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/widgets/artwork/cover.ui")]
    pub struct Cover {
        #[template_child]
        pub image: TemplateChild<gtk::Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Cover {
        const NAME: &'static str = "Cover";
        type Type = super::Cover;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Cover {}
    impl WidgetImpl for Cover {}
}

glib::wrapper! {
    pub struct Cover(ObjectSubclass<imp::Cover>)
        @extends gtk::Widget;
}

impl Cover {
    pub fn new() -> Self {
        let object = glib::Object::new::<Cover>();
        //let imp = imp::Cover::from_obj(&object);
        object
    }

    pub fn set_image(&self, filepath: &str, size: i32) {
        let this = self.imp();
        let path = std::path::Path::new(filepath);
        this.image.set_pixel_size(size);
        this.image.set_from_file(Some(path));
    }
}
