// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::subclass::prelude::*;
use glib::prelude::IsA;
use gtk::{gio, glib};

use crate::engine::bus::{BusMessageImpl, Message};
use crate::widgets::content::Content;
use crate::widgets::sidebar::SideBar;

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/application/window.ui")]
    pub struct ApplicationWindow {
        #[template_child]
        pub sidebar: TemplateChild<SideBar>,
        #[template_child]
        pub content: TemplateChild<Content>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ApplicationWindow {
        const NAME: &'static str = "ApplicationWindow";
        type Type = super::ApplicationWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ApplicationWindow {}
    impl WidgetImpl for ApplicationWindow {}
    impl WindowImpl for ApplicationWindow {}
    impl ApplicationWindowImpl for ApplicationWindow {}
    impl AdwApplicationWindowImpl for ApplicationWindow {}
}

glib::wrapper! {
    pub struct ApplicationWindow(ObjectSubclass<imp::ApplicationWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl ApplicationWindow {
    pub fn new<P: IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::builder()
            .property("application", application)
            .build()
    }
}

impl BusMessageImpl for ApplicationWindow {
    fn handle(&self, message: &Message) {
        let this = self.imp();
        this.sidebar.handle(message);
        this.content.handle(message);
    }
}
