// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use super::scanner;
use gtk::gio;
use gtk::prelude::*;

#[derive(Debug)]
pub enum Action {
    CollectionSelected(i32),
}

#[derive(Debug)]
pub enum Event {}

#[derive(Debug)]
pub enum Message {
    Event(Event),
    Action(Action),
    Scanner(scanner::Message),
}

pub trait BusMessageImpl {
    fn handle(&self, message: &Message) {
        self.dispatch(message);
    }

    fn dispatch(&self, message: &Message) {
        match message {
            Message::Action(a) => {
                self.action(&a);
            }
            Message::Event(e) => {
                self.event(&e);
            }
            Message::Scanner(m) => {
                self.scanner(&m);
            }
        }
    }
    fn action(&self, _action: &Action) {}
    fn event(&self, _event: &Event) {}
    fn scanner(&self, _message: &scanner::Message) {}
}

pub fn send_message(message: Message) {
    let app = gio::Application::default()
        .expect("Failed to retrieve application singleton")
        .downcast::<crate::application::Application>()
        .unwrap();
    match &*app.sender().read().unwrap() {
        Some(sender) => {
            sender.send_blocking(message).ok();
        }
        None => {}
    }
}
