// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

/// Return if assertion fails
#[macro_export]
macro_rules! assert_ret {
    ($x:ident $op:tt $y:expr, $ret: expr) => {
        if $x $op $y {
            return $expr;
        }
    };
    ($x:ident $op:tt $y:expr) => {
        if $x $op $y {
            return;
        }
    };
}

/// Get unix timestamp
#[macro_export]
macro_rules! timestamp {
    ($now: expr) => {
        match $now.duration_since(std::time::UNIX_EPOCH) {
            // Don't want to handle errors, if this fail, use my birthday!
            // 17/03/1981 => Cédric Bellegarde
            Err(_error) => 353703900,
            Ok(timestamp) => timestamp.as_secs(),
        }
    };
}

/// Split strings in vector by char
#[macro_export]
macro_rules! split_vec_strings {
    ($vec: expr, $ch: expr) => {{
        let mut new_vec = Vec::new();
        for item in $vec.iter() {
            for str_ in item.split($ch) {
                if str_ != "" {
                    new_vec.push(String::from(str_));
                }
            }
        }
        new_vec
    }};
}

/// Loop: continue if Option is None
#[macro_export]
macro_rules! unwrap_or_continue {
    ($res:expr) => {
        match $res {
            Some(val) => val,
            None => {
                continue;
            }
        }
    };
}

/// Loop: continue if Result contains an error
#[macro_export]
macro_rules! unwrap_error_or_continue {
    ($res:expr) => {
        match $res {
            Ok(val) => val,
            Err(error) => {
                log::error!("{:?}", error);
                continue;
            }
        }
    };
}

#[macro_export]
macro_rules! trim {
    ($string:expr, str) => {
        $string.trim()
    };
    ($string:expr, Option<str>) => {
        match &$string {
            Some(value) => Some(value.trim()),
            None => None,
        }
    };
}

#[macro_export]
macro_rules! vec_remove {
    ($vec: expr, $value: expr) => {
        match $vec.iter().position(|x| *x == $value) {
            Some(index) => {
                $vec.remove(index);
            }
            None => {
                log::error!("vec_remove! Failed to find item")
            }
        }
    };
}
