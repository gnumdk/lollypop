// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use gtk::gio;
use gtk::prelude::FileExt;
use log::error;
use std::ffi::OsStr;
use std::path::Path;

#[derive(PartialEq)]
pub enum FileType {
    UNKNOWN = 0,
    AUDIO = 1,
    PLS = 2,
    OTHER = 3,
}

/// Get file type for URI
///
/// # Parameters
///
/// * `uri`: target URI
pub fn file_type(uri: &String) -> FileType {
    let audio = vec![
        "3gp", "aa", "aac", "aax", "act", "aiff", "alac", "amr", "ape", "au", "awb", "dct", "dss",
        "dvf", "flac", "gsm", "iklax", "ivs", "m4a", "m4b", "m4p", "mmf", "mp3", "mpc", "msv",
        "nmf", "nsf", "ogg", "opus", "ra", "raw", "rf64", "sln", "tta", "voc", "vox", "wav", "wma",
        "wv", "webm", "8svx", "cda",
    ];
    let other = vec![
        "7z", "arj", "deb", "pkg", "rar", "rpm", "tar.gz", "z", "zip", "pdf", "doc", "docx", "xls",
        "xlsx", "ppt", "pptx", "db", "txt", "mov", "avi", "html", "ini", "cue", "nfo", "lrc",
    ];
    let image = vec![
        "ai", "bmp", "gif", "ico", "jpeg", "jpg", "png", "ps", "psd", "svg", "tif",
    ];
    let pls = vec!["pls", "m3u"];
    match Path::new(uri).extension().and_then(OsStr::to_str) {
        Some(ext) => {
            if audio.contains(&ext) {
                return FileType::AUDIO;
            } else if pls.contains(&ext) {
                return FileType::PLS;
            } else if image.contains(&ext) || other.contains(&ext) {
                return FileType::OTHER;
            } else {
                return file_type_from_content(uri);
            }
        }
        None => {
            return FileType::UNKNOWN;
        }
    }
}

/// Get file type for URI content
///
/// # Parameters
///
/// * `uri`: target URI
pub fn file_type_from_content(uri: &String) -> FileType {
    let file = gio::File::for_uri(uri);
    match file.query_info(
        "standard::content-type",
        gio::FileQueryInfoFlags::NONE,
        gio::Cancellable::NONE,
    ) {
        Ok(info) => {
            if is_pls(&info) {
                return FileType::PLS;
            } else if is_audio(&info) {
                return FileType::AUDIO;
            } else {
                return FileType::UNKNOWN;
            }
        }
        Err(error) => {
            error!(
                "liblollypop::utils::files::file_type_from_content: {}",
                error
            );
            return FileType::UNKNOWN;
        }
    }
}

/// True if file is a playlist
///
/// # Parameters
///
/// * `info`: file information as gio::FileInfo
pub fn is_pls(info: &gio::FileInfo) -> bool {
    match info.content_type() {
        Some(content_type) => {
            return vec![
                "audio/x-mpegurl",
                "application/xspf+xml",
                "application/vnd.apple.mpegurl",
            ]
            .contains(&content_type.as_str());
        }
        None => {
            return false;
        }
    }
}

/// True if file is audio
///
/// # Parameters
///
/// * `info`: file information as gio::FileInfo
pub fn is_audio(info: &gio::FileInfo) -> bool {
    match info.content_type() {
        Some(content_type) => {
            return vec![
                "application/ogg",
                "application/x-ogg",
                "application/x-ogm-audio",
                "audio/aac",
                "audio/mp4",
                "audio/mpeg",
                "audio/mpegurl",
                "audio/ogg",
                "audio/vnd.rn-realaudio",
                "audio/vorbis",
                "audio/x-flac",
                "audio/x-mp3",
                "audio/x-mpeg",
                "audio/x-mpegurl",
                "audio/x-ms-wma",
                "audio/x-musepack",
                "audio/x-oggflac",
                "audio/x-pn-realaudio",
                "application/x-flac",
                "audio/x-speex",
                "audio/x-vorbis",
                "audio/x-vorbis+ogg",
                "audio/x-wav",
                "x-content/audio-player",
                "audio/x-aac",
                "audio/m4a",
                "audio/x-m4a",
                "audio/mp3",
                "audio/ac3",
                "audio/flac",
                "audio/x-opus+ogg",
                "application/x-extension-mp4",
                "audio/x-ape",
                "audio/x-pn-aiff",
                "audio/x-pn-au",
                "audio/x-pn-wav",
                "audio/x-pn-windows-acm",
                "application/x-matroska",
                "audio/x-matroska",
                "audio/x-wavpack",
                "video/mp4",
                "audio/x-mod",
                "audio/x-mo3",
                "audio/x-xm",
                "audio/x-s3m",
                "audio/x-it",
                "audio/aiff",
                "audio/x-aiff",
            ]
            .contains(&content_type.as_str());
        }
        None => {
            return false;
        }
    }
}
