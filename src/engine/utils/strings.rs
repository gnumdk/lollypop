// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

/// Return string without accents lowered
pub fn no_accents(slice: &str) -> String {
    any_ascii::any_ascii(slice).to_lowercase()
}

/// Return string without accents lowered
pub fn no_accents_i(slice: &str) -> String {
    any_ascii::any_ascii(slice)
        .to_lowercase()
        .chars()
        .filter(|c| c.is_alphanumeric())
        .collect()
}
