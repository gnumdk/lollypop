// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Extract data from a tag frame

use crate::engine::utils::bytes::{as_u32_le, fix_utf16, remove_utf16_sep, split};
use encoding_rs;
use log::error;
use std::convert::TryInto;
use std::str;

/// Encoding is latin1
const LATIN1_ENCODING: &u8 = &b'\x00';
/// Encoding is UTF-16
const UTF_16_ENCODING: &u8 = &b'\x01';
/// Enoding is UTF-16 BE
const UTF_16BE_ENCODING: &u8 = &b'\x02';
/// Encoding is UTF-8
const UTF_8_ENCODING: &u8 = &b'\x03';

#[doc(hidden)]
const LOG: &str = "engine::tags::tagframe";

/// Synced lyrics with timestamp and related sentence
#[derive(Clone)]
pub struct SyncedLyrics {
    /// Lyrics timestamp
    pub timestamp: u32,
    /// Lyrics content
    pub content: String,
}

/// Parse a tag frame
pub struct TagFrameParser<'a> {
    #[doc(hidden)]
    bytes: &'a [u8],
}

/// Extract data from bytes
impl<'a> TagFrameParser<'a> {
    /// Get a new tag frame
    /// # Parameters
    /// * `bytes`: frame as bytes
    /// # Returns
    /// A new tag frame
    pub fn new(bytes: &'a [u8]) -> Self {
        Self { bytes }
    }

    /// Get tag frame as text
    /// # Returns
    /// Tag frame string
    pub fn text(&self) -> String {
        let encoding = self.encoding();
        match &encoding {
            LATIN1_ENCODING | UTF_8_ENCODING => {
                let parts = split(self.body(), &[0]);
                if let Some(last) = parts.last() {
                    let part = last.to_vec();
                    if &encoding == LATIN1_ENCODING {
                        let mut utf8 = vec![0; part.len() * 2];
                        let count =
                            encoding_rs::mem::convert_latin1_to_utf8(&part, utf8.as_mut_slice());
                        return String::from_utf8_lossy(&utf8.as_slice()[..count]).into_owned();
                    } else {
                        return String::from_utf8_lossy(&part).into_owned();
                    }
                }
            }
            UTF_16_ENCODING | UTF_16BE_ENCODING => {
                let parts: Vec<&[u8]>;
                if &encoding == UTF_16_ENCODING {
                    parts = split(self.body(), &[255, 254]);
                } else {
                    parts = split(self.body(), &[0, 0]);
                }
                if let Some(last) = parts.last() {
                    let mut part = last.to_vec();
                    fix_utf16(&mut part);
                    let utf16: Vec<u16> = part
                        .chunks_exact(2)
                        .into_iter()
                        .map(|a| u16::from_be_bytes([a[0], a[1]]))
                        .collect();
                    return String::from_utf16_lossy(&utf16.as_slice());
                }
            }
            _ => {}
        }
        String::new()
    }

    /// Get tag frame as integer
    /// # Returns
    /// Tag frame as integer
    pub fn integer(&self) -> u8 {
        return self.bytes[11];
    }

    /// Get synced lyrics from tag frame
    /// # Returns
    /// An array of synced lyrics
    pub fn synced_lyrics(&self) -> Vec<SyncedLyrics> {
        let mut synced_lyrics: Vec<SyncedLyrics> = Vec::new();
        let mut lines: Vec<&[u8]> = Vec::new();
        let encoding = self.encoding();
        match &encoding {
            LATIN1_ENCODING | UTF_8_ENCODING => {
                lines = split(self.body(), &[10]);
            }
            UTF_16_ENCODING => {
                lines = split(self.body(), &[10, 0]);
            }
            UTF_16BE_ENCODING => {
                lines = split(self.body(), &[0, 10]);
            }
            _ => {}
        }
        match &encoding {
            LATIN1_ENCODING | UTF_8_ENCODING => {
                let mut index = 1;
                while index < lines.len() {
                    let line = lines[index];
                    let synced: SyncedLyrics;
                    let timestamp: u32;
                    let length = line.len();
                    if length < 5 {
                        continue;
                    }
                    match line[length - 5..length - 1].try_into() {
                        Ok(value) => {
                            timestamp = as_u32_le(value);
                        }
                        Err(error) => {
                            error!("{}::synced_lyrics: {}", LOG, error);
                            continue;
                        }
                    }
                    let lyrics = &line[0..line.len() - 5];
                    if &encoding == LATIN1_ENCODING {
                        let mut utf8 = vec![0; lyrics.len() * 2];
                        let count =
                            encoding_rs::mem::convert_latin1_to_utf8(&lyrics, utf8.as_mut_slice());
                        synced = SyncedLyrics {
                            timestamp,
                            content: String::from_utf8_lossy(&utf8.as_slice()[..count])
                                .into_owned(),
                        };
                    } else {
                        synced = SyncedLyrics {
                            timestamp,
                            content: String::from_utf8_lossy(&lyrics).into_owned(),
                        };
                    }
                    synced_lyrics.push(synced);
                    index += 1;
                }
            }
            UTF_16_ENCODING | UTF_16BE_ENCODING => {
                let mut index = 1;
                while index < lines.len() {
                    let line = remove_utf16_sep(lines[index]);
                    let timestamp: u32;
                    let length = line.len();
                    if length < 5 {
                        continue;
                    }
                    match line[length - 5..length - 1].try_into() {
                        Ok(value) => {
                            timestamp = as_u32_le(value);
                        }
                        Err(error) => {
                            error!("{}::synced_lyrics: {}", LOG, error);
                            continue;
                        }
                    }
                    let mut lyrics = Vec::from(&line[0..line.len() - 6]);
                    fix_utf16(&mut lyrics);
                    let utf16: Vec<u16> = lyrics
                        .chunks_exact(2)
                        .into_iter()
                        .map(|a| u16::from_be_bytes([a[0], a[1]]))
                        .collect();
                    let synced = SyncedLyrics {
                        timestamp,
                        content: String::from_utf16_lossy(&utf16.as_slice()),
                    };
                    synced_lyrics.push(synced);
                    index += 1;
                }
            }
            _ => {}
        }

        synced_lyrics
    }

    /// Get frame body
    /// # Returns
    /// Frame body bytes
    pub fn body(&self) -> &[u8] {
        &self.bytes[11..]
    }

    /// Get frame key
    /// # Returns
    /// Frame key
    pub fn key(&self) -> String {
        match str::from_utf8(&self.bytes[0..4]) {
            Ok(value) => {
                return String::from(value);
            }
            Err(_error) => {
                return String::new();
            }
        }
    }

    /// Get frame encoding
    /// # Returns
    /// Encoding byte
    pub fn encoding(&self) -> u8 {
        self.bytes[10]
    }
}
