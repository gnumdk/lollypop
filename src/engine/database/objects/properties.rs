// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! This trait is used to load objects properties
use imp::PropertiesLoader;

use super::super::sql::builder::Object;
use super::super::sql::connection::Sql;
use crate::{to_option, to_value};

use std::collections::HashMap;

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum Property {
    Id,            // All
    Name,          // Artist
    Title,         // Album, Disc, Track
    Number,        // Disc, Track
    Sortname,      // Artist
    Synced,        // Album
    Mbid,          // Album, Artist
    Uri,           // Album, Track
    Popularity,    // Album, Track
    Status,        // Album, Track
    Rate,          // Album, Track
    Mtime,         // Track
    Year,          // ALbum, Track
    Timestamp,     // Album, Track
    Duration,      // Track
    Ltime,         // Track
    IsCompilation, // Album
    Bpm,           // Track
    DiscId,        // Track
    AlbumId,       // Track, Disc
}

pub trait Convertor {
    fn convert(value: &rusqlite::types::Value, property: Property) -> Self
    where
        Self: Sized;
}

impl Convertor for i64 {
    fn convert(value: &rusqlite::types::Value, property: Property) -> i64 {
        match property {
            Property::Number
            | Property::Synced
            | Property::Popularity
            | Property::Status
            | Property::Rate
            | Property::Mtime
            | Property::Year
            | Property::Timestamp
            | Property::Duration
            | Property::Ltime => {
                to_value!(value, i64)
            }
            _ => {
                panic!("{:?} is not an i64", property);
            }
        }
    }
}

impl Convertor for Option<i64> {
    fn convert(value: &rusqlite::types::Value, property: Property) -> Option<i64> {
        match property {
            Property::Bpm => {
                to_option!(value, i64)
            }
            _ => {
                panic!("{:?} is not an Option<i64>", property);
            }
        }
    }
}

impl Convertor for bool {
    fn convert(value: &rusqlite::types::Value, property: Property) -> bool {
        match property {
            Property::IsCompilation => {
                to_value!(value, bool)
            }
            _ => {
                panic!("{:?} is not a bool", property);
            }
        }
    }
}

impl Convertor for String {
    fn convert(value: &rusqlite::types::Value, property: Property) -> String {
        match property {
            Property::Id
            | Property::Name
            | Property::Title
            | Property::Sortname
            | Property::Uri
            | Property::DiscId
            | Property::AlbumId => {
                to_value!(value, str)
            }
            _ => {
                panic!("{:?} is not a string", property);
            }
        }
    }
}

impl Convertor for Option<String> {
    fn convert(value: &rusqlite::types::Value, property: Property) -> Option<String> {
        match property {
            Property::Mbid => {
                to_option!(value, str)
            }
            _ => {
                panic!("{:?} is not an Option<String>", property);
            }
        }
    }
}

pub struct Properties<'a> {
    sql: &'a dyn Sql<'a>,
    object: Object,
    values: HashMap<Property, rusqlite::types::Value>,
}

impl<'a> Properties<'a> {
    pub fn new(sql: &'a dyn Sql<'a>, id: String, object: Object) -> Properties {
        let mut properties = Properties {
            sql,
            object,
            values: HashMap::new(),
        };
        properties
            .values
            .insert(Property::Id, rusqlite::types::Value::Text(id));
        properties
    }

    pub fn set(&mut self, property: Property, value: rusqlite::types::Value) {
        self.values.insert(property, value);
    }

    pub fn reset(&mut self) {
        let id = self.values.remove(&Property::Id).unwrap();
        self.values.clear();
        self.values.insert(Property::Id, id);
        self.load();
    }

    pub fn get<U>(&self, property: Property) -> U
    where
        U: Convertor,
    {
        match self.values.get(&property) {
            Some(value) => Convertor::convert(value, property),
            None => {
                panic!("Property not available");
            }
        }
    }
}

#[doc(hidden)]
mod imp {
    use super::super::super::sql::builder::{Query, Select, Where};
    use super::super::super::sql::connection::{Sql, SqliteQuery};
    use super::Object;
    use super::{to_value, Properties, Property};

    use rusqlite::params;

    pub trait PropertiesLoader {
        fn load(&mut self);
    }

    impl<'a> dyn PropertiesLoader + 'a {
        fn genre_properties(&self, sql: &dyn Sql, id: String) -> Vec<rusqlite::types::Value> {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql("genres.name"),
                where_: Some(Where {
                    sql: "genres.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            sql.row(&query.sql(), params![id])
        }

        fn artist_properties(&self, sql: &dyn Sql, id: String) -> Vec<rusqlite::types::Value> {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql("artists.name, sortname, mbid"),
                where_: Some(Where {
                    sql: "artists.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            sql.row(&query.sql(), params![id])
        }

        fn album_properties(&self, sql: &dyn Sql, id: String) -> Vec<rusqlite::types::Value> {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql(
                    "albums.title, mbid, uri, popularity, status, rate,\
                         year, timestamp, is_compilation, synced",
                ),
                where_: Some(Where {
                    sql: "albums.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            sql.row(&query.sql(), params![id])
        }

        fn disc_properties(&self, sql: &dyn Sql, id: String) -> Vec<rusqlite::types::Value> {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql("discs.title, album_id, number"),
                where_: Some(Where {
                    sql: "discs.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            sql.row(&query.sql(), params![id])
        }

        fn track_properties(&self, sql: &dyn Sql, id: String) -> Vec<rusqlite::types::Value> {
            let mut query = Query {
                // Keep order in sync with Property
                select: Select::Sql(
                    "albums.title, number, mbid, uri, popularity, status, rate, mtime,\
                         year, timestamp, duration, ltime, album_id, disc_id, bpm",
                ),
                where_: Some(Where {
                    sql: "albums.id=?",
                    ..Default::default()
                }),
                ..Default::default()
            };
            sql.row(&query.sql(), params![id])
        }
    }

    impl<'a> PropertiesLoader for Properties<'a> {
        fn load(&mut self) {
            let id = to_value!(self.values.get(&Property::Id).unwrap(), str);
            let loader: &dyn PropertiesLoader = self as &dyn PropertiesLoader;
            match self.object {
                Object::Genre => {
                    let mut values = loader.genre_properties(self.sql, id);
                    self.values.insert(Property::Name, values.pop().unwrap());
                }
                Object::Artist => {
                    let mut values = loader.artist_properties(self.sql, id);
                    self.values.insert(
                        Property::Mbid,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                    self.values.insert(
                        Property::Sortname,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                    self.values.insert(
                        Property::Name,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                }
                Object::Album => {
                    let mut values = loader.album_properties(self.sql, id);
                    self.values.insert(
                        Property::Synced,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::IsCompilation,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Timestamp,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Year,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Rate,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Status,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Popularity,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Uri,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                    self.values.insert(
                        Property::Mbid,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                    self.values.insert(
                        Property::Title,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                }
                Object::Disc => {
                    let mut values = loader.disc_properties(self.sql, id);
                    self.values.insert(
                        Property::Number,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::AlbumId,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                    self.values.insert(
                        Property::Title,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                }
                Object::Track => {
                    let mut values = loader.track_properties(self.sql, id);
                    self.values.insert(
                        Property::Bpm,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::DiscId,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                    self.values.insert(
                        Property::AlbumId,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                    self.values.insert(
                        Property::Ltime,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Duration,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Timestamp,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Year,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Mtime,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Rate,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Status,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Popularity,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Uri,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                    self.values.insert(
                        Property::Mbid,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                    self.values.insert(
                        Property::Number,
                        values.pop().unwrap_or(rusqlite::types::Value::Integer(0)),
                    );
                    self.values.insert(
                        Property::Title,
                        values
                            .pop()
                            .unwrap_or(rusqlite::types::Value::Text(String::new())),
                    );
                }
            }
        }
    }
}
