//! Database objects

pub mod album;
pub mod artist;
pub mod disc;
pub mod genre;
pub mod properties;
pub mod relations;
pub mod track;
