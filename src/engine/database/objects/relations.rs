// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! This trait is used to load objects relations

use super::super::objects::album::Album;
use super::super::objects::artist::Artist;
use super::super::objects::disc::Disc;
use super::super::objects::track::Track;
use std::collections::HashMap;

/// Relation between objects in database
#[derive(PartialEq, Eq, Hash)]
pub enum Relation {
    ArtistIds,
    PerformerIds,
    ConductorIds,
    RemixerIds,
    ComposerIds,
    GenreIds,
    AlbumIds,
    DiscIds,
    TrackIds,
}

#[doc(hidden)]
macro_rules! impl_relations_loader {
    () => {
        fn relation(&mut self, relation: Relation) -> Vec<String> {
            // Read lock here
            {
                if let Some(ids) = self.relations.get(&relation) {
                    return ids.clone();
                }
            }
            // Write lock here
            imp::Relations::load_relations(self, &relation);
            match self.relations.get(&relation) {
                Some(ids) => ids.clone(),
                None => Vec::new(),
            }
        }
    };
}

#[doc(hidden)]
pub trait Relations {
    fn relation(&mut self, relation: Relation) -> Vec<String>;
}

impl<'a> Relations for Artist<'a> {
    impl_relations_loader![];
}
impl<'a> Relations for Album<'a> {
    impl_relations_loader![];
}
impl<'a> Relations for Disc<'a> {
    impl_relations_loader![];
}
impl<'a> Relations for Track<'a> {
    impl_relations_loader![];
}

#[doc(hidden)]
mod imp {
    use super::super::super::filters::{Filter, Filters};
    use super::super::super::objects::genre::Genre;
    use super::super::super::objects::properties::{Properties, Property};
    use super::super::super::sql::builder::{Object, Query, Select, Where};
    use super::super::super::sql::connection::SqliteQuery;
    use super::HashMap;
    use super::{Album, Artist, Disc, Track};

    use log::warn;
    use rusqlite::params;

    #[doc(hidden)]
    macro_rules! impl_relations_getter {
        () => {
            fn relations(&mut self) -> &mut HashMap<super::Relation, Vec<String>> {
                &mut self.relations
            }
        };
    }

    pub trait Relations {
        fn relations(&mut self) -> &mut HashMap<super::Relation, Vec<String>>;
        fn load_relations(&mut self, relation: &super::Relation) {
            match relation {
                super::Relation::GenreIds => {
                    let genre_ids = self.genre_ids();
                    self.relations()
                        .insert(super::Relation::GenreIds, genre_ids);
                }
                super::Relation::ArtistIds => {
                    let artist_ids = self.artist_ids();
                    self.relations()
                        .insert(super::Relation::ArtistIds, artist_ids);
                }
                super::Relation::AlbumIds => {
                    let album_ids = self.album_ids();
                    self.relations()
                        .insert(super::Relation::AlbumIds, album_ids);
                }
                super::Relation::DiscIds => {
                    let disc_ids = self.disc_ids();
                    self.relations().insert(super::Relation::DiscIds, disc_ids);
                }
                super::Relation::TrackIds => {
                    let track_ids = self.track_ids();
                    self.relations()
                        .insert(super::Relation::TrackIds, track_ids);
                }
                _ => {
                    warn!("Tracks::add_relation() called with invalid Relation");
                }
            }
        }
        fn genre_ids(&mut self) -> Vec<String> {
            vec![]
        }
        fn artist_ids(&mut self) -> Vec<String> {
            vec![]
        }
        fn album_ids(&mut self) -> Vec<String> {
            vec![]
        }
        fn disc_ids(&mut self) -> Vec<String> {
            vec![]
        }
        fn track_ids(&mut self) -> Vec<String> {
            vec![]
        }
    }
    impl<'a> Relations for Genre<'a> {
        impl_relations_getter![];
        fn artist_ids(&mut self) -> Vec<String> {
            let mut filters = Filters::new();
            filters.add(Filter::Genres(vec![self
                .properties
                .get::<String>(Property::Id)]));
            let mut query = Query {
                select: Select::Object(Object::Artist),
                where_: Some(Where {
                    filters: Some(filters),
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            self.sql.strings(&query.sql(), params![])
        }
    }
    impl<'a> Relations for Artist<'a> {
        impl_relations_getter![];
        fn album_ids(&mut self) -> Vec<String> {
            let mut filters = self.filters.clone().unwrap_or(Filters::new());
            filters.add(Filter::Artists(vec![self
                .properties
                .get::<String>(Property::Id)]));
            let mut query = Query {
                select: Select::Object(Object::Album),
                where_: Some(Where {
                    filters: Some(filters),
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            self.sql.strings(&query.sql(), &query.params())
        }
    }
    impl<'a> Relations for Album<'a> {
        impl_relations_getter![];
        // Get disc ids
        fn disc_ids(&mut self) -> Vec<String> {
            let mut query = Query {
                select: Select::Object(Object::Disc),
                where_: Some(Where {
                    sql: "discs.album_id=?",
                    filters: self.filters.clone(),
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            self.sql.strings(
                &query.sql(),
                params![self.properties.get::<String>(Property::Id)],
            )
        }
    }
    impl<'a> Relations for Disc<'a> {
        impl_relations_getter![];
        fn artist_ids(&mut self) -> Vec<String> {
            let mut query = Query {
                select: Select::Sql("disc_artists.artist_id"),
                where_: Some(Where {
                    sql: "disc_artists.disc_id=?",
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            self.sql.strings(
                &query.sql(),
                params![self.properties.get::<String>(Property::Id)],
            )
        }

        fn track_ids(&mut self) -> Vec<String> {
            let mut query = Query {
                select: Select::Object(Object::Track),
                where_: Some(Where {
                    sql: "tracks.disc_id=?",
                    filters: self.filters.clone(),
                    ..Default::default()
                }),
                distinct: true,
                ..Default::default()
            };
            self.sql.strings(
                &query.sql(),
                params![
                    self.properties.get::<String>(Property::Id),
                    self.properties.get::<String>(Property::AlbumId)
                ],
            )
        }
    }
    impl<'a> Relations for Track<'a> {
        impl_relations_getter![];
    }
}
