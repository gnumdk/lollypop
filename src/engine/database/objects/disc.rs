// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Represent a collection disc

use super::super::filters::Filters;
use super::super::objects::properties::Properties;
use super::super::objects::relations::Relation;
use super::super::sql::builder::Object;
use super::super::sql::connection::Sql;

use std::collections::HashMap;

/// A disc
pub struct Disc<'a> {
    // Database SQL connection
    pub sql: &'a dyn Sql<'a>,
    // True if skipped tracks are shown
    pub skipped_tracks: bool,
    // Properties
    pub properties: Properties<'a>,
    // Relations
    pub relations: HashMap<Relation, Vec<String>>,
    // Filters
    pub filters: Option<Filters>,
}

impl<'a> Disc<'a> {
    /// Construct a new disc
    /// # Parameters
    ///
    /// * `sql`: a database SQL connection
    /// * `id`: a disc id
    /// * `filters`: applied filters
    /// # Returns
    /// A new disc
    pub fn new(sql: &'a dyn Sql<'a>, id: String, filters: Option<Filters>) -> Disc<'a> {
        let disc = Disc {
            sql,
            filters,
            skipped_tracks: true,
            properties: Properties::new(sql, id, Object::Disc),
            relations: HashMap::new(),
        };
        disc
    }
}
