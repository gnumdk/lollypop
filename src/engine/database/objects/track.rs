// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Represent a collection track

use super::super::filters::Filters;
use super::super::objects::properties::Properties;
use super::super::objects::relations::Relation;
use super::super::sql::builder::Object;
use super::super::sql::connection::Sql;

use std::collections::HashMap;

/// A track
pub struct Track<'a> {
    // Database SQL connection
    pub sql: &'a dyn Sql<'a>,
    // Properties
    pub properties: Properties<'a>,
    // Relations
    pub relations: HashMap<Relation, Vec<String>>,
    // Filters
    pub filters: Option<Filters>,
}

impl<'a> Track<'a> {
    /// Construct a new track
    /// # Parameters
    /// * `sql`: a database SQL connection
    /// * `id`: a track id
    /// * `filters`: applied filters
    /// # Returns
    /// A new track
    pub fn new(sql: &'a dyn Sql<'a>, id: String, filters: Option<Filters>) -> Track<'a> {
        let track = Track {
            sql,
            filters,
            properties: Properties::new(sql, id, Object::Track),
            relations: HashMap::new(),
        };
        track
    }
}
