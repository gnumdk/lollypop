// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Represent a collection genre

use super::super::objects::properties::{Properties, Property};
use super::super::objects::relations::Relation;
use super::super::sql::builder::Object;
use super::super::sql::connection::Sql;

use std::collections::HashMap;

/// A genre
pub struct Genre<'a> {
    // Database SQL connection
    pub sql: &'a dyn Sql<'a>,
    // Properties
    pub properties: Properties<'a>,
    // Relations
    pub relations: HashMap<Relation, Vec<String>>,
}

impl<'a> Genre<'a> {
    /// Construct a new genre
    /// # Parameters
    /// * `sql`: a database SQL connection
    /// * `id`: a genre id
    /// # Returns
    /// A new genre
    pub fn new(sql: &'a dyn Sql<'a>, id: String) -> Genre<'a> {
        let genre = Genre {
            sql,
            properties: Properties::new(sql, id, Object::Genre),
            relations: HashMap::new(),
        };
        genre
    }
}

impl std::fmt::Debug for Genre<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Genre")
            //.field(&self.id)
            .field(&self.properties.get::<String>(Property::Name))
            .finish()
    }
}
