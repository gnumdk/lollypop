// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Represent an album

use super::super::filters::Filters;
use super::super::objects::properties::{Properties, Property};
use super::super::objects::relations::Relation;
use super::super::sql::builder::{Object, OrderBy, Query, Select};
use super::super::sql::connection::{Sql, SqliteQuery};

use rusqlite::params;
use std::cmp::max;
use std::collections::HashMap;

/// An album
pub struct Album<'a> {
    /// Database SQL connection
    pub sql: &'a dyn Sql<'a>,
    /// Use (crate::property) or [`property_option`](crate::property_option)
    pub properties: Properties<'a>,
    /// Use [`Relations`](crate::engine::database::objects::relations::Relations)
    pub relations: HashMap<Relation, Vec<String>>,
    /// Filters applied to object Relations
    pub filters: Option<Filters>,
}

impl<'a> Album<'a> {
    /// Construct a new album
    /// # Parameters
    /// * `sql`: a database SQL connection
    /// * `id`: an album id
    /// * `filters`: applied filters
    /// # Returns
    /// A new album
    pub fn new(sql: &'a dyn Sql<'a>, id: String, filters: Option<Filters>) -> Album<'a> {
        let album = Album {
            sql,
            filters,
            properties: Properties::new(sql, id, Object::Album),
            relations: HashMap::new(),
        };
        album
    }

    /// Set album popularity
    /// # Parameters
    /// * `popularity`: album popularity
    pub fn set_popularity(&mut self, popularity: i64) {
        assert!(self.sql.writable());

        self.properties.set(
            Property::Popularity,
            rusqlite::types::Value::Integer(popularity),
        );
        self.sql
            .execute(
                "UPDATE albums set popularity=? WHERE id=?",
                params![popularity, self.properties.get::<i64>(Property::Id)],
            )
            .ok();
    }

    /// Set album more popular
    pub fn set_more_popular(&mut self) {
        assert!(self.sql.writable());

        let mut max_query = Query {
            select: Select::Sql("albums.popularity"),
            orderby: Some(OrderBy::Custom("POPULARITY DESC")),
            limit: Some(1),
            ..Default::default()
        };
        let max_popularity = self.sql.integer(&max_query.sql(), params![]);
        let increment = (max_popularity - self.properties.get::<i64>(Property::Popularity)) / 100;
        self.set_popularity(max(1, increment));
    }
}
