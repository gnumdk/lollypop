// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Represent a collection artist

use super::super::filters::Filters;
use super::super::objects::properties::Properties;
use super::super::objects::relations::Relation;
use super::super::sql::builder::Object;
use super::super::sql::connection::Sql;

use std::collections::HashMap;

/// An artist
pub struct Artist<'a> {
    // Database SQL connection
    pub sql: &'a dyn Sql<'a>,
    // Properties
    pub properties: Properties<'a>,
    // Relations
    pub relations: HashMap<Relation, Vec<String>>,
    // Filters
    pub filters: Option<Filters>,
}

impl<'a> Artist<'a> {
    /// Construct a new artist
    /// # Parameters
    /// * `sql`: a database SQL connection
    /// * `id`: an artist id
    /// * `filters`: applied filters
    /// # Returns
    /// A new artist
    pub fn new(sql: &'a dyn Sql<'a>, id: String, filters: Option<Filters>) -> Artist<'a> {
        let artist = Artist {
            sql,
            filters,
            properties: Properties::new(sql, id, Object::Artist),
            relations: HashMap::new(),
        };
        artist
    }
}
