// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Database Macros

/// Get values at path
#[macro_export]
macro_rules! filtered_values {
    ($filters: expr, $matching: path) => {
        match &*$filters {
            Some(filters) => {
                for filter in filters {
                    match filter {
                        $matching(values) => {
                            return values.to_vec();
                        }
                        _ => (),
                    }
                }
            }
            None => (),
        }
        vec![]
    };
}
