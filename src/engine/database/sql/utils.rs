// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use super::super::storage_path;

use log::{debug, error};
use rusqlite::Connection;

/// Compares the C string a to the C string b
/// # Parameters
/// `a`: a string
/// `b`: a string
/// # Returns
/// Strings ordering
pub fn strcoll(a: &str, b: &str) -> std::cmp::Ordering {
    let c_a = std::ffi::CString::new(a).expect("CString::new failed");
    let c_b = std::ffi::CString::new(b).expect("CString::new failed");
    let result: i32;
    unsafe {
        result = libc::strcoll(c_a.as_ptr(), c_b.as_ptr());
    }
    if result > 0 {
        std::cmp::Ordering::Greater
    } else if result < 0 {
        std::cmp::Ordering::Less
    } else {
        std::cmp::Ordering::Equal
    }
}

/// Create a localized collation on connection
/// This collation use [`strcoll`] to sort items
/// # Parameters
/// `connection`: a SQLite connection
pub fn localized_collation(connection: &Connection) {
    connection.create_collation("LOCALIZED", strcoll).ok();
}

/// Create a no_accents collation on connection
/// This collation use [`crate::engine::utils::strings::no_accents`] to filter items
/// # Parameters
/// `connection`: a SQLite connection
pub fn no_accents(connection: &Connection) {
    if let Err(error) = connection.create_scalar_function(
        "no_accents",
        1,
        rusqlite::functions::FunctionFlags::SQLITE_UTF8
            | rusqlite::functions::FunctionFlags::SQLITE_DETERMINISTIC,
        |ctx| {
            let value = ctx.get::<String>(0).unwrap();
            Ok(crate::engine::utils::strings::no_accents(value.as_str()))
        },
    ) {
        error!("Can't create no_accents() function: {}", error);
    }
}

/// Create a no_accents_i collation on connection
/// This collation use [`crate::engine::utils::strings::no_accents_i`] to filter items
/// # Parameters
/// `connection`: a SQLite connection
pub fn no_accents_i(connection: &Connection) {
    if let Err(error) = connection.create_scalar_function(
        "no_accents_i",
        1,
        rusqlite::functions::FunctionFlags::SQLITE_UTF8
            | rusqlite::functions::FunctionFlags::SQLITE_DETERMINISTIC,
        |ctx| {
            let value = ctx.get::<String>(0).unwrap();
            Ok(crate::engine::utils::strings::no_accents_i(value.as_str()))
        },
    ) {
        error!("Can't create no_accents_i() function: {}", error);
    }
}

/// Get a new database connection
/// # Returns
/// A new connection
pub fn connection() -> Connection {
    let path = format!("{}/collection.db", storage_path::COLLECTION.as_str());
    match Connection::open(&path) {
        Ok(connection) => {
            localized_collation(&connection);
            no_accents(&connection);
            no_accents_i(&connection);
            connection
        }
        _ => {
            panic!("Failed to access {}!", path);
        }
    }
}
