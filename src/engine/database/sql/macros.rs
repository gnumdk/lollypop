// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! SQL Macros

/// Get [`rusqlite::ToSql`] params from [`Vec<rusqlite::types::Value>`]
#[macro_export]
macro_rules! params_from_values {
    () => {
        Vec<&dyn rusqlite::ToSql>::new()
    };
    ($values:expr) => {{
        let mut params: Vec<&dyn rusqlite::ToSql> = Vec::new();
        for value in $values.iter() {
            params.push(value as &dyn rusqlite::ToSql);
        }
        params
    }};
}

/// Get [`rusqlite::types::Value`] as inner type
#[macro_export]
macro_rules! to_value {
    ($x:expr, bool) => {
        match $x {
            rusqlite::types::Value::Integer(value) => *value != 0,
            _ => false,
        }
    };
    ($x:expr, i64) => {
        match $x {
            &rusqlite::types::Value::Integer(value) => value,
            _ => 0,
        }
    };
    ($x:expr, f64) => {
        match $x {
            &rusqlite::types::Value::Real(value) => value,
            _ => 0.0,
        }
    };
    ( $x:expr, str) => {
        match $x {
            rusqlite::types::Value::Text(value) => value.to_string(),
            _ => String::new(),
        }
    };
}

/// Try to get [`rusqlite::types::Value`] as inner type
#[macro_export]
macro_rules! to_option {
    ($x:expr, i64) => {
        match $x {
            &rusqlite::types::Value::Integer(value) => Some(value),
            _ => None,
        }
    };
    ($x:expr, f64) => {
        match $x {
            &rusqlite::types::Value::Real(value) => Some(value),
            _ => None,
        }
    };
    ($x:expr, str) => {
        match $x {
            rusqlite::types::Value::Text(value) => Some(value.to_string()),
            _ => None,
        }
    };
}

// Split strings in Vec as anothers Vec items
#[macro_export]
macro_rules! split_tags_semicolon {
    ($vec: expr) => {{
        let mut new_vec = Vec::new();
        for genre in $vec.iter() {
            for str_ in genre.split(";") {
                if str_ != "" {
                    new_vec.push(String::from(str_));
                }
            }
        }
        new_vec
    }};
}
