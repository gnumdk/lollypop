// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Sql thread safe Connection and Transaction

use crate::engine::collection::Collection;

use crate::{to_option, to_value};

use rusqlite::ToSql;
use std::sync::{RwLockReadGuard, RwLockWriteGuard};

/// Lock guard for default collection
pub enum LockGuard<'a> {
    /// Read access to collection
    Read(RwLockReadGuard<'a, Collection>),
    /// Write access to collection
    Write(RwLockWriteGuard<'a, Collection>),
}

pub struct Connection<'a> {
    /// SQLite database connection
    connection: rusqlite::Connection,
    /// Database lock access via collection
    lock_guard: LockGuard<'a>,
}

pub struct Transaction<'a> {
    /// SQLite database transaction
    transaction: rusqlite::Transaction<'a>,
    /// Database lock access via collection
    lock_guard: LockGuard<'a>,
}

#[doc(hidden)]
pub trait Sql<'a> {
    /// Get SQLite connection
    fn sqlite_connection(&self) -> &rusqlite::Connection;
    /// Execute query
    fn execute(&self, sql: &str, params: &[&dyn ToSql]) -> Result<i64, String>;
    /// Check we are writable
    fn writable(&self) -> bool;
}

/// SQLite query
pub trait SqliteQuery {
    /// Get string for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// A string (empty if no result available)
    fn string(&self, query: &String, params: &[&dyn ToSql]) -> String;

    /// Try to get a string for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// A string or None if not result available
    fn try_string(&self, query: &String, params: &[&dyn ToSql]) -> Option<String>;

    /// Get integer for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// An integer (0 if no result available)
    fn integer(&self, query: &String, params: &[&dyn ToSql]) -> i64;

    /// Try to get an integer for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// An integer or None if not result available
    fn try_integer(&self, query: &String, params: &[&dyn ToSql]) -> Option<i64>;

    /// Get float for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// A float (0.0 if no result available)
    fn float(&self, query: &String, params: &[&dyn ToSql]) -> f64;

    /// Try to get a float for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// A float or None if not result available
    fn try_float(&self, query: &String, params: &[&dyn ToSql]) -> Option<f64>;

    /// Get strings for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// An array of strings
    fn strings(&self, query: &String, params: &[&dyn ToSql]) -> Vec<String>;

    /// Get integers for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// An array of integers
    fn integers(&self, query: &String, params: &[&dyn ToSql]) -> Vec<i64>;

    /// Get floats for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// An array of floats
    fn floats(&self, query: &String, params: &[&dyn ToSql]) -> Vec<f64>;

    /// Get row for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// An array of rusqlite values
    fn row(&self, query: &String, params: &[&dyn ToSql]) -> Vec<rusqlite::types::Value>;

    /// Get rows for query
    /// # Parameters
    /// * `query`: sql query
    /// * `params` : sql parameters
    /// # Returns
    /// An array of rows
    fn rows(&self, query: &String, params: &[&dyn ToSql]) -> Vec<Vec<rusqlite::types::Value>>;
}

impl<'a> Connection<'a> {
    pub fn ro(connection: rusqlite::Connection) -> Connection<'a> {
        Self::new(connection, false)
    }

    pub fn rw(connection: rusqlite::Connection) -> Connection<'a> {
        Self::new(connection, true)
    }

    fn new(connection: rusqlite::Connection, writable: bool) -> Connection<'a> {
        let lock_guard = if writable == true {
            LockGuard::Write(Collection::write())
        } else {
            LockGuard::Read(Collection::read())
        };

        Connection {
            connection,
            lock_guard,
        }
    }
}

impl<'a> Transaction<'a> {
    /// Get a new transaction
    /// # Parameters
    /// * `transaction`: SQLite transaction
    pub fn new(mut transaction: rusqlite::Transaction<'a>) -> Transaction<'a> {
        transaction.set_drop_behavior(rusqlite::DropBehavior::Commit);
        Transaction {
            transaction,
            lock_guard: LockGuard::Write(Collection::write()),
        }
    }
}

#[doc(hidden)]
impl<'a> dyn Sql<'_> + 'a {
    // Get row
    fn get_row(
        &self,
        connection: &rusqlite::Connection,
        query: &String,
        params: &[&dyn ToSql],
    ) -> Vec<rusqlite::types::Value> {
        let mut values = Vec::new();
        let mut stmt = match connection.prepare(query) {
            Ok(stmt) => stmt,
            Err(_) => return values,
        };
        let column_count = stmt.column_count();
        let mut rows = match stmt.query(params) {
            Ok(rows) => rows,
            Err(_) => return values,
        };
        let row = match rows.next() {
            Ok(Some(row)) => row,
            Ok(None) => return values,
            Err(_) => return values,
        };
        for i in 0..column_count {
            match row.get_ref_unwrap(i) {
                rusqlite::types::ValueRef::Null => values.push(rusqlite::types::Value::Null),
                rusqlite::types::ValueRef::Integer(int) => {
                    values.push(rusqlite::types::Value::Integer(int))
                }
                rusqlite::types::ValueRef::Real(real) => {
                    values.push(rusqlite::types::Value::Real(real))
                }
                rusqlite::types::ValueRef::Text(utf8) => match String::from_utf8_lossy(utf8) {
                    std::borrow::Cow::Borrowed(value) => {
                        values.push(rusqlite::types::Value::Text(String::from(value)));
                    }
                    std::borrow::Cow::Owned(value) => {
                        values.push(rusqlite::types::Value::Text(value.to_owned().to_string()));
                    }
                },
                rusqlite::types::ValueRef::Blob(_blob) => {
                    log::error!("Not implemented")
                }
            }
        }
        values
    }

    // Get rows
    fn get_rows(
        &self,
        connection: &rusqlite::Connection,
        query: &String,
        params: &[&dyn ToSql],
    ) -> Vec<Vec<rusqlite::types::Value>> {
        let mut items = Vec::new();
        let mut stmt = match connection.prepare(query) {
            Ok(stmt) => stmt,
            Err(_) => return items,
        };
        let column_count = stmt.column_count();
        let mut rows = match stmt.query(params) {
            Ok(rows) => rows,
            Err(_) => return items,
        };
        while let Ok(Some(row)) = rows.next() {
            let mut values = Vec::new();
            for i in 0..column_count {
                match row.get_ref_unwrap(i) {
                    rusqlite::types::ValueRef::Null => {
                        values.push(rusqlite::types::Value::Null);
                    }
                    rusqlite::types::ValueRef::Integer(int) => {
                        values.push(rusqlite::types::Value::Integer(int));
                    }
                    rusqlite::types::ValueRef::Real(real) => {
                        values.push(rusqlite::types::Value::Real(real));
                    }
                    rusqlite::types::ValueRef::Text(utf8) => match String::from_utf8_lossy(utf8) {
                        std::borrow::Cow::Borrowed(value) => {
                            values.push(rusqlite::types::Value::Text(String::from(value)));
                        }
                        std::borrow::Cow::Owned(value) => {
                            values.push(rusqlite::types::Value::Text(value.to_owned().to_string()));
                        }
                    },
                    rusqlite::types::ValueRef::Blob(_blob) => {
                        log::error!("Not implemented")
                    }
                }
            }
            items.push(values);
        }
        items
    }
}

impl<'a> Sql<'a> for Connection<'a> {
    fn sqlite_connection(&self) -> &rusqlite::Connection {
        &self.connection
    }

    fn execute(&self, sql: &str, params: &[&dyn ToSql]) -> Result<i64, String> {
        self.connection
            .execute(sql, params)
            .map(|_i| -> i64 { self.connection.last_insert_rowid() })
            .map_err(|e| -> String { format!("{}: {}", e, sql) })
    }

    fn writable(&self) -> bool {
        match self.lock_guard {
            LockGuard::Read(_) => false,
            LockGuard::Write(_) => true,
        }
    }
}

impl<'a> Sql<'a> for Transaction<'a> {
    fn sqlite_connection(&self) -> &rusqlite::Connection {
        &*self.transaction
    }

    fn execute(&self, sql: &str, params: &[&dyn ToSql]) -> Result<i64, String> {
        self.transaction
            .execute(sql, params)
            .map(|_i| -> i64 { self.transaction.last_insert_rowid() })
            .map_err(|e| -> String { format!("{}: {}", e, sql) })
    }

    fn writable(&self) -> bool {
        true
    }
}

impl<'a> SqliteQuery for dyn Sql<'_> + 'a {
    fn string(&self, query: &String, params: &[&dyn ToSql]) -> String {
        let columns = self.get_row(self.sqlite_connection(), query, params);
        if columns.len() > 0 {
            return to_value!(&columns[0], str);
        }
        String::new()
    }

    fn try_string(&self, query: &String, params: &[&dyn ToSql]) -> Option<String> {
        let columns = self.get_row(self.sqlite_connection(), query, params);
        if columns.len() > 0 {
            return to_option!(&columns[0], str);
        }
        None
    }

    fn integer(&self, query: &String, params: &[&dyn ToSql]) -> i64 {
        let columns = self.get_row(self.sqlite_connection(), query, params);
        if columns.len() > 0 {
            return to_value!(&columns[0], i64);
        }
        0
    }

    fn try_integer(&self, query: &String, params: &[&dyn ToSql]) -> Option<i64> {
        let columns = self.get_row(self.sqlite_connection(), query, params);
        if columns.len() > 0 {
            return to_option!(&columns[0], i64);
        }
        None
    }

    fn float(&self, query: &String, params: &[&dyn ToSql]) -> f64 {
        let columns = self.get_row(self.sqlite_connection(), query, params);
        if columns.len() > 0 {
            return to_value!(&columns[0], f64);
        }
        0.0
    }

    fn try_float(&self, query: &String, params: &[&dyn ToSql]) -> Option<f64> {
        let columns = self.get_row(self.sqlite_connection(), query, params);
        if columns.len() > 0 {
            return to_option!(&columns[0], f64);
        }
        None
    }

    fn strings(&self, query: &String, params: &[&dyn ToSql]) -> Vec<String> {
        let mut strings = Vec::new();
        for result in self.get_rows(self.sqlite_connection(), query, params) {
            for column in result {
                if let rusqlite::types::Value::Text(string) = column {
                    strings.push(string);
                    break;
                }
            }
        }
        strings
    }

    fn integers(&self, query: &String, params: &[&dyn ToSql]) -> Vec<i64> {
        let mut integers = Vec::new();
        for result in self.get_rows(self.sqlite_connection(), query, params) {
            for column in result {
                if let rusqlite::types::Value::Integer(integer) = column {
                    integers.push(integer);
                    break;
                }
            }
        }
        integers
    }

    fn floats(&self, query: &String, params: &[&dyn ToSql]) -> Vec<f64> {
        let mut floats = Vec::new();
        for result in self.get_rows(self.sqlite_connection(), query, params) {
            for column in result {
                if let rusqlite::types::Value::Real(real) = column {
                    floats.push(real);
                    break;
                }
            }
        }
        floats
    }

    fn row(&self, query: &String, params: &[&dyn ToSql]) -> Vec<rusqlite::types::Value> {
        self.get_row(self.sqlite_connection(), query, params)
    }

    fn rows(&self, query: &String, params: &[&dyn ToSql]) -> Vec<Vec<rusqlite::types::Value>> {
        self.get_rows(self.sqlite_connection(), query, params)
    }
}
