// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Filters for database queries

use std::mem;
use std::ops::{Deref, DerefMut};

/// A database filter
#[derive(Clone, PartialEq, Debug)]
pub enum Filter {
    /// Filter on Genre/Artist/Album/Disc/Track id
    Id(String),
    /// Filter on Genre/Artist/Album/Disc/Track name
    Name(Option<String>),
    /// Filter on Genre/Artist/Album/Disc/Track name ignoring accents
    NoAccentName(Option<String>),
    /// Filter on Genre/Artist/Album/Disc/Track name ignoring accents and case
    NoAccentNameI(Option<String>),
    Title(Option<String>),
    NoAccentTitle(Option<String>),
    NoAccentTitleI(Option<String>),
    Mbid(Option<String>),
    Number(Option<i64>),
    Timestamp(Option<i64>),
    Uri(Option<String>),
    Album(Option<i64>),
    Genres(Vec<String>),
    Artists(Vec<String>),
    Discs(Vec<String>),
}

#[derive(Clone, Debug)]
pub struct Filters {
    #[doc(hidden)]
    values: Vec<Filter>,
}

impl Filters {
    /// Construct new empty Filters
    /// # Returns
    /// New filters
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    /// Add or override a filter
    /// # Parameters
    /// `filter`: filter to add
    pub fn add(&mut self, filter: Filter) {
        let mut new_values = vec![];
        for value in &self.values {
            if mem::discriminant(value) != mem::discriminant(&filter) {
                new_values.push(value.clone());
            }
        }
        // Ignore empty values
        match &filter {
            Filter::Id(value) => {
                if value == "" {
                    return;
                };
            }
            Filter::Genres(value) | Filter::Artists(value) | Filter::Discs(value) => {
                if value.len() == 0 {
                    return;
                };
            }
            _ => {}
        }
        new_values.push(filter);
        self.values = new_values
    }

    /// Check if filter is empty
    /// # Returns
    /// true if filter is empty
    pub fn is_empty(&self) -> bool {
        self.values.len() == 0
    }
}

impl Default for Filters {
    fn default() -> Self {
        Self { values: vec![] }
    }
}

impl Deref for Filters {
    type Target = Vec<Filter>;

    fn deref(&self) -> &Self::Target {
        &self.values
    }
}

impl DerefMut for Filters {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.values
    }
}

/// Remove id from filters at path
macro_rules! remove_from_filters {
    ($filters: expr, $id :expr, $path: path) => {{
        let mut new_filters = Filters::new();
        for filter in &*$filters {
            if let $path(ids) = filter {
                let mut new_ids = vec![];
                for id in ids {
                    if !id.eq($id) {
                        new_ids.push(id.clone());
                    }
                }
                if new_ids.len() > 0 {
                    new_filters.push($path(new_ids));
                }
            } else {
                new_filters.push(filter.clone());
            }
        }
        new_filters
    }};
}
