// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Collection database

use std::sync::LazyLock;

pub mod filters;
pub mod macros;
pub mod methods;
pub mod objects;
pub mod sql;

/// Track/Album status flags
pub enum StatusFlags {
    /// No flags
    NONE = 1 << 0,
    /// Track/Album is loved
    LOVED = 1 << 1,
    /// Track is skipped
    SKIPPED = 1 << 2,
    /// All above status
    ALL = 1 << 0 | 1 << 1 | 1 << 2,
}

/// Various storage paths
pub mod storage_path {
    use super::LazyLock;
    /// Collection databases
    pub static COLLECTION: LazyLock<String> = LazyLock::new(|| {
        let path = gtk::glib::user_data_dir();
        format!("{}/lollypop/database", path.to_string_lossy())
    });
    /// Artist artworks
    pub static ARTWORK_ARTISTS: LazyLock<String> = LazyLock::new(|| {
        let path = gtk::glib::user_data_dir();
        format!("{}/lollypop/artwork/artists", path.to_string_lossy())
    });
    /// Album artworks
    pub static ARTWORK_ALBUMS: LazyLock<String> = LazyLock::new(|| {
        let path = gtk::glib::user_data_dir();
        format!("{}/lollypop/artwork/albums", path.to_string_lossy())
    });
    /// Lyrics as text files
    pub static LYRICS: LazyLock<String> = LazyLock::new(|| {
        let path = gtk::glib::user_data_dir();
        format!("{}/lollypop/lyrics", path.to_string_lossy())
    });
    /// Artworks cache for albums
    pub static CACHE_ALBUMS: LazyLock<String> = LazyLock::new(|| {
        let path = gtk::glib::user_cache_dir();
        format!("{}/lollypop/albums", path.to_string_lossy())
    });
    /// Artworks cache for artists
    pub static CACHE_ARTISTS: LazyLock<String> = LazyLock::new(|| {
        let path = gtk::glib::user_cache_dir();
        format!("{}/lollypop/artists", path.to_string_lossy())
    });
}
