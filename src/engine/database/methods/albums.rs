// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Albums database access methods

use super::super::filters::{Filter, Filters};
use super::super::objects::properties::Property;
use super::super::sql::builder::{Object, OrderBy, Query, Select, Where};
use super::super::sql::connection::{Sql, SqliteQuery};
use super::super::StatusFlags;
use crate::engine::tags::item::Item;
use crate::{timestamp, to_option, to_value, trim};

use log::error;
use rusqlite::params;
use std::collections::HashMap;
use std::time::SystemTime;

/// Add a new album to database
/// # Parameters
/// * `sql`: Connection to database
/// * `properties`: Album properties
/// # Returns
/// Album id
pub fn add(
    sql: &dyn Sql,
    properties: &HashMap<Property, rusqlite::types::Value>,
    artist_ids: &Vec<String>,
) -> Option<String> {
    assert!(sql.writable());

    let title = to_option!(
        properties
            .get(&Property::Title)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let year = to_option!(
        properties
            .get(&Property::Year)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let timestamp = to_option!(
        properties
            .get(&Property::Timestamp)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let mbid = to_option!(
        properties
            .get(&Property::Mbid)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let is_compilation = to_value!(
        properties
            .get(&Property::IsCompilation)
            .unwrap_or(&rusqlite::types::Value::Null),
        bool
    );
    let uri = to_value!(
        properties
            .get(&Property::Uri)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let status = to_value!(
        properties
            .get(&Property::Status)
            .unwrap_or(&rusqlite::types::Value::Integer(StatusFlags::NONE as i64)),
        i64
    );
    let popularity = to_value!(
        properties
            .get(&Property::Popularity)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let rate = to_value!(
        properties
            .get(&Property::Rate)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let synced = to_value!(
        properties
            .get(&Property::Synced)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let album_id_str = format!(
        "{:?}_{:?}_{:?}_{:?}",
        title,
        mbid,
        year,
        artist_ids.clone().sort()
    );
    let album_id = format!("{:x}", md5::compute(album_id_str.to_lowercase().as_bytes()));
    if exists(sql, &album_id) {
        return Some(album_id);
    }
    let query = "INSERT INTO albums (
                 id, title, mbid, is_compilation, uri, year,
                 timestamp, status, popularity, rate, synced)
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    match sql.execute(
        &query,
        params![
            album_id,
            trim!(title, Option<str>),
            trim!(mbid, Option<str>),
            is_compilation,
            uri,
            year,
            timestamp,
            status,
            popularity,
            rate,
            synced
        ],
    ) {
        Err(error) => {
            error!(
                "Failed to add album: {}/{}, {:?}, {:?}, {}, {}, {:?}, {:?}, {}, {}, {}, {} ({})",
                query,
                album_id,
                title,
                mbid,
                is_compilation,
                uri,
                year,
                timestamp,
                status,
                popularity,
                rate,
                synced,
                error
            );
            None
        }
        Ok(_) => Some(album_id),
    }
}

/// Add a new album item to database
/// # Parameters
/// * `sql`: Connection to database
/// * `item`: Album item
/// * `artist_ids`: album artist ids
/// # Returns
/// Album id
pub fn add_item(sql: &dyn Sql, item: &Item, artist_ids: &Vec<String>) -> Option<String> {
    assert!(sql.writable());

    add(sql, &item.properties, artist_ids)
}

/// Check album exists in database
/// # Parameters
/// * `sql`: Connection to database
/// * `id`: Album id
/// # Returns
/// true if album id exists
pub fn exists(sql: &dyn Sql, id: &str) -> bool {
    let mut filters = Filters::new();
    filters.add(Filter::Id(String::from(id)));
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            filters: Some(filters),
            ..Default::default()
        }),
        ..Default::default()
    };
    let result = sql.try_string(&query.sql(), query.params().as_slice());
    result != None
}

/// Get album ids from database
/// # Parameters
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `allow_compilations`: return compilations in results
/// * `status_flags`: Album status
/// * `limit`: Results limit
/// # Returns
/// Album ids
pub fn ids(
    sql: &dyn Sql,
    filters: Option<Filters>,
    orderby: Option<OrderBy>,
    allow_compilations: bool,
    status_flags: StatusFlags,
    limit: Option<i64>,
) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        params: vec![
            rusqlite::types::Value::Integer(allow_compilations as i64),
            rusqlite::types::Value::Integer(status_flags as i64),
        ],
        where_: Some(Where {
            sql: "albums.is_compilation=? \
                      AND albums.status&?",
            filters,
            ..Default::default()
        }),
        orderby,
        limit,
        distinct: true,
        ..Default::default()
    };
    log::warn!("{:?}", query.sql());
    sql.strings(&query.sql(), &query.params())
}

/// Get album ids with rate >= 4 from database
/// # Parameters
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `status_flags`: Album status
/// * `limit`: Results limit
/// # Returns
/// Album ids with rate >= 4
pub fn rated_ids(
    sql: &dyn Sql,
    filters: Option<Filters>,
    status_flags: i64,
    limit: Option<i64>,
) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "rate>=4 AND albums.status&?",
            filters,
            ..Default::default()
        }),
        limit,
        orderby: Some(OrderBy::Custom("rate DESC")),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![status_flags])
}

/// Get album ids with popularity != 0 from database
/// # Parameters
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `status_flags`: Album status
/// * `limit`: Results limit
/// # Returns
/// Popular album ids
pub fn popular_ids(
    sql: &dyn Sql,
    filters: Option<Filters>,
    status_flags: i64,
    limit: Option<i64>,
) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "popularity!=0 AND albums.status&?",
            filters,
            ..Default::default()
        }),
        limit,
        orderby: Some(OrderBy::Custom("popularity DESC")),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![status_flags])
}

/// Get album ids ordered by timed popularity from database
/// # Parameters
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `status_flags`: Album status
/// * `limit`: Results limit
/// # Returns
/// Currently popular Album ids
pub fn popular_ids_currently(
    sql: &dyn Sql,
    filters: Option<Filters>,
    status_flags: i64,
    limit: Option<i64>,
) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "albums.id = albums_timed_popularity.album_id AND albums.status&?",
            filters,
            ..Default::default()
        }),
        orderby: Some(OrderBy::Custom("albums_timed_popularity.popularity DESC")),
        limit,
        ..Default::default()
    };
    sql.strings(&query.sql(), params![status_flags])
}

/// Search for albums in database
/// # Parameters
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `limit`: Results limit
/// # Returns
/// Matching album ids
pub fn search(sql: &dyn Sql, filter: &str, limit: i64) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "no_accents(title) LIKE %?%",
            ..Default::default()
        }),
        limit: Some(limit),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![filter])
}

/// Get albums count from database
/// # Parameters
/// * `sql`: Connection to database
/// # Returns
/// Albums count
pub fn count(sql: &dyn Sql) -> i64 {
    let mut query = Query {
        select: Select::Sql("COUNT(albums.*)"),
        ..Default::default()
    };
    sql.integer(&query.sql(), params![])
}

/// Clean albums table in database
/// # Parameters
/// * `sql`: Connection to database
pub fn clean(sql: &dyn Sql) {
    assert!(sql.writable());

    sql.execute(
        "DELETE FROM albums
            WHERE albums.id NOT IN (
                SELECT album_id FROM tracks)",
        params![],
    )
    .ok();

    sql.execute(
        "DELETE FROM albums_timed_popularity
               WHERE albums_timed_popularity.album_id NOT IN (
                SELECT albums.id FROM albums)",
        params![],
    )
    .ok();

    // We clear timed popularity based on mtime
    // Don't keep more data than a month
    let month = timestamp![SystemTime::now()] - 2678400;
    sql.execute(
        "DELETE FROM albums_timed_popularity
               WHERE albums_timed_popularity.mtime < ?",
        params![month as i64],
    )
    .ok();
}
