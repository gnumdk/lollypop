//! Database getters/setters

pub mod albums;
pub mod artists;
pub mod discs;
pub mod genres;
pub mod tracks;
