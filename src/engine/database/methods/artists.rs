// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Artists database access methods

use super::super::filters::{Filter, Filters};
use super::super::objects::properties::Property;
use super::super::sql::builder::{Object, OrderBy, Query, Select, Where};
use super::super::sql::connection::{Sql, SqliteQuery};
use crate::engine::tags::item::Item;
use crate::{to_option, trim};
use log::error;
use rusqlite::params;
use std::collections::HashMap;

/// Add a new artist to database
/// # Parameters
/// * `sql`: Connection to database
/// * `properties`: Artist properties
/// # Returns
/// Artist id
pub fn add(
    sql: &dyn Sql,
    properties: &HashMap<Property, rusqlite::types::Value>,
) -> Option<String> {
    assert!(sql.writable());

    // As Option to prevent inserting empty values (NOT NULL)
    let name = to_option!(
        properties
            .get(&Property::Name)
            .unwrap_or(&rusqlite::types::Value::Text(String::from("Unknown"))),
        str
    );
    let mbid = to_option!(
        properties
            .get(&Property::Mbid)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    // As Option to prevent inserting empty values (NOT NULL)
    let sortname = to_option!(
        properties
            .get(&Property::Sortname)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let artist_id_str = format!("{:?}_{:?}", name, mbid);
    let artist_id = format!(
        "{:x}",
        md5::compute(artist_id_str.to_lowercase().as_bytes())
    );
    if exists(sql, &artist_id) {
        return Some(artist_id);
    }
    let query = "INSERT INTO artists (id, name, sortname, mbid) VALUES (?, ?, ?, ?)";
    match sql.execute(
        &query,
        params![
            artist_id,
            trim!(name, Option<str>),
            trim!(sortname, Option<str>),
            mbid
        ],
    ) {
        Err(error) => {
            error!(
                "Failed to add artist: {}/{}, {:?}, {:?}, {:?} ({})",
                query, artist_id, name, sortname, mbid, error
            );
            None
        }
        Ok(_) => Some(artist_id),
    }
}

/// Add a new artist item to database
/// # Parameters
/// * `sql`: Connection to database
/// * `item`: Artist item
/// # Returns
/// Artist id
pub fn add_item(sql: &dyn Sql, item: &Item) -> Option<String> {
    assert!(sql.writable());

    add(sql, &item.properties)
}

/// Check artist exists in database
///
/// # Parameters
///
/// * `sql`: Connection to database
/// * `id`: Artist id
/// # Returns
/// true if artist exists
pub fn exists(sql: &dyn Sql, id: &str) -> bool {
    let mut filters = Filters::new();
    filters.add(Filter::Id(String::from(id)));
    let mut query = Query {
        select: Select::Object(Object::Artist),
        where_: Some(Where {
            filters: Some(filters),
            ..Default::default()
        }),
        ..Default::default()
    };
    let result = sql.try_string(&query.sql(), query.params().as_slice());
    result != None
}

/// Get artist ids from database
/// # Parameters
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `limit`: Results limit
/// # Returns
/// Artist ids
pub fn ids(
    sql: &dyn Sql,
    filters: Option<Filters>,
    orderby: Option<OrderBy>,
    limit: Option<i64>,
) -> Vec<String> {
    // TODO Add an option to show artists from compilations
    // TODO Add an option to not use detected compilations
    let mut query = Query {
        select: Select::Object(Object::Artist),
        where_: Some(Where {
            sql: "albums.id=discs.album_id \
                 AND discs.id=disc_artists.disc_id \
                 AND artists.id=disc_artists.artist_id",
            filters,
            ..Default::default()
        }),
        orderby,
        limit,
        distinct: true,
        ..Default::default()
    };
    sql.strings(&query.sql(), &query.params())
}

/// Get random artists from database
/// # Parameters
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `limit`: Results limit
/// # Returns
/// Artist ids
pub fn randoms(sql: &dyn Sql, filters: Option<Filters>, limit: Option<i64>) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Artist),
        where_: Some(Where {
            filters,
            ..Default::default()
        }),
        limit,
        ..Default::default()
    };
    sql.strings(&query.sql(), params![])
}

/// Get albums containing artist in featuring from database
/// # Parameters
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `orderby`: Ordering
/// * `limit`: Results limit
/// # Returns
/// Album ids
pub fn featured(
    sql: &dyn Sql,
    filters: Option<Filters>,
    orderby: Option<OrderBy>,
    limit: Option<i64>,
) -> Vec<String> {
    let mut query = Query {
        select: Select::Sql("featuring.album_id"),
        where_: Some(Where {
            sql: "artists.id=featuring.artist_id \
                  AND albums.id=featuring.album_id",
            filters,
            ..Default::default()
        }),
        orderby,
        limit,
        ..Default::default()
    };
    sql.strings(&query.sql(), params![])
}

/// Get artists count from database
/// # Parameters
/// * `sql`: Connection to database
/// # Returns
/// Artists count
pub fn count(sql: &dyn Sql) -> i64 {
    let mut query = Query {
        select: Select::Sql("COUNT(artist.id)"),
        ..Default::default()
    };
    sql.integer(&query.sql(), params![])
}

/// Clean artists table in database
/// # Parameters
/// * `sql`: Connection to database
pub fn clean(sql: &dyn Sql) {
    assert!(sql.writable());

    sql.execute(
        "DELETE FROM artists WHERE artists.id NOT IN (
                SELECT disc_artists.artist_id
                FROM disc_artists)
            AND artists.id NOT IN (
                SELECT track_artists.artist_id
                FROM track_artists)",
        params![],
    )
    .ok();
}
