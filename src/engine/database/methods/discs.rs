// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Discs database access methods

use super::super::filters::{Filter, Filters};
use super::super::objects::properties::Property;
use super::super::objects::relations::Relation;
use super::super::sql::builder::{Object, Query, Select, Where};
use super::super::sql::connection::{Sql, SqliteQuery};
use crate::engine::tags::item::DiscItem;
use crate::{to_option, to_value, trim};

use log::{error, warn};
use rusqlite::params;
use std::collections::HashMap;

/// Add a new disc to database
/// # Parameters
/// * `sql`: Connection to database
/// * `properties`: Disc properties
/// # Returns
/// Disc id
pub fn add(
    sql: &dyn Sql,
    properties: &HashMap<Property, rusqlite::types::Value>,
) -> Option<String> {
    assert!(sql.writable());

    let title = to_option!(
        properties
            .get(&Property::Title)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let number = to_value!(
        properties
            .get(&Property::Number)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let album_id = to_value!(
        properties
            .get(&Property::AlbumId)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let disc_id_str = format!("{:?}_{}_{}", title, number, album_id);
    let disc_id = format!("{:x}", md5::compute(disc_id_str.to_lowercase().as_bytes()));
    if exists(sql, &disc_id) {
        return Some(disc_id);
    }
    let query = "INSERT INTO discs (id, title, number, album_id) VALUES (?, ?, ?, ?)";
    match sql.execute(
        &query,
        params![disc_id, trim!(title, Option<str>), number, album_id],
    ) {
        Err(error) => {
            error!(
                "Failed to add disc: {}/{},{:?},{},{} ({})",
                query, disc_id, title, number, album_id, error
            );
            None
        }
        Ok(_) => Some(disc_id),
    }
}

/// Add relation to disc
/// # Parameters
/// * `sql`: Connection to database
/// * `disc_id`: Target disc id
/// * `ids`: ids Related ids
/// * `relation`: Relation
pub fn add_relation(sql: &dyn Sql, disc_id: &String, ids: &Vec<String>, relation: Relation) {
    assert!(sql.writable());

    match relation {
        Relation::ArtistIds => {
            for artist_id in ids {
                let query = format!(
                    "REPLACE INTO disc_artists (disc_id, artist_id)
                                     VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![disc_id, artist_id]) {
                    Err(error) => error!("Failed to add artist to disc: {}", error),
                    Ok(_rowid) => {}
                }
            }
        }
        Relation::GenreIds => {
            for genre_id in ids {
                let query = format!(
                    "REPLACE INTO disc_genres (disc_id, genre_id)
                                     VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![disc_id, genre_id]) {
                    Err(error) => error!("Failed to add genre to disc: {}", error),
                    Ok(_rowid) => {}
                }
            }
        }
        _ => warn!("Discs::add_relation() called with invalid Relation"),
    }
}

/// Add a new album item to database
/// # Parameters
/// * `sql`: Connection to database
/// * `item`: Disc item
/// # Returns
/// Disc id
pub fn add_item(sql: &dyn Sql, item: &DiscItem) -> Option<String> {
    assert!(sql.writable());

    add(sql, &item.properties)
}

/// Check disc exists in database
/// # Parameters
/// * `sql`: Connection to database
/// * `id`: Disc id
/// # Returns
/// true if exists
pub fn exists(sql: &dyn Sql, id: &str) -> bool {
    let mut filters = Filters::new();
    filters.add(Filter::Id(String::from(id)));
    let mut query = Query {
        select: Select::Object(Object::Disc),
        where_: Some(Where {
            filters: Some(filters),
            ..Default::default()
        }),
        ..Default::default()
    };
    let result = sql.try_string(&query.sql(), query.params().as_slice());
    result != None
}

/// Clean discs table in database
/// # Parameters
/// * `sql`: Connection to database
pub fn clean(sql: &dyn Sql) {
    assert!(sql.writable());

    sql.execute(
        "DELETE FROM discs WHERE discs.id NOT IN (
         SELECT disc_id FROM tracks)",
        params![],
    )
    .ok();
    sql.execute(
        "DELETE FROM disc_genres
            WHERE disc_genres.disc_id NOT IN (
                SELECT discs.id FROM discs)",
        params![],
    )
    .ok();
    sql.execute(
        "DELETE FROM disc_artists
            WHERE disc_artists.disc_id NOT IN (
                SELECT discs.id FROM discs)",
        params![],
    )
    .ok();
}
