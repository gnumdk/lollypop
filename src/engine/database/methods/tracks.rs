// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Tracks database access methods

use super::super::objects::properties::Property;
use super::super::objects::relations::Relation;
use super::super::sql::builder::{Object, OrderBy, Query, Select, Where};
use super::super::sql::connection::{Sql, SqliteQuery};
use crate::engine::tags::item::TrackItem;
use crate::{to_option, to_value, trim};

use log::{error, warn};
use rusqlite::params;
use std::cmp::max;
use std::collections::HashMap;

/// Add a new track to database
/// # Parameters
/// * `sql`: Connection to database
/// * `properties`: Track properties
/// # Returns
/// Track id
pub fn add(
    sql: &dyn Sql,
    properties: &HashMap<Property, rusqlite::types::Value>,
) -> Option<String> {
    assert!(sql.writable());

    // As Option to prevent inserting empty values (NOT NULL)
    let title = to_option!(
        properties
            .get(&Property::Title)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let year = to_option!(
        properties
            .get(&Property::Year)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let mbid = to_option!(
        properties
            .get(&Property::Mbid)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let disc_id = to_value!(
        properties
            .get(&Property::DiscId)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let duration = to_value!(
        properties
            .get(&Property::Duration)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    // As Option to prevent inserting empty values (NOT NULL)
    let uri = to_option!(
        properties
            .get(&Property::Uri)
            .unwrap_or(&rusqlite::types::Value::Null),
        str
    );
    let tracknumber = to_option!(
        properties
            .get(&Property::Number)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let timestamp = to_option!(
        properties
            .get(&Property::Timestamp)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let status = to_value!(
        properties
            .get(&Property::Status)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let popularity = to_value!(
        properties
            .get(&Property::Popularity)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let rate = to_value!(
        properties
            .get(&Property::Rate)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let mtime = to_value!(
        properties
            .get(&Property::Mtime)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let ltime = to_value!(
        properties
            .get(&Property::Ltime)
            .unwrap_or(&rusqlite::types::Value::Null),
        i64
    );
    let bpm = to_option!(
        properties
            .get(&Property::Bpm)
            .unwrap_or(&rusqlite::types::Value::Null),
        f64
    );
    let track_id_str = format!("{:?}_{:?}_{}_{}", title, year, disc_id, duration);
    let track_id = format!("{:x}", md5::compute(track_id_str.to_lowercase().as_bytes()));
    let query = "INSERT INTO tracks (
             id, title, mbid, uri, number, year,
             timestamp, status, popularity, rate, mtime,
             ltime, duration, bpm, disc_id)
             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    match sql.execute(
        &query,
        params![
            track_id,
            trim!(title, Option<str>),
            trim!(mbid, Option<str>),
            uri,
            tracknumber,
            year,
            timestamp,
            status,
            popularity,
            rate,
            mtime,
            ltime,
            duration,
            bpm,
            disc_id
        ],
    ) {
        Err(error) => {
            error!(
                "Failed to add track: {}/{}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {}, {}, {}, {}, {}, {}, {:?}, {}, ({})",
                query, track_id,
                    title,
                    mbid,
                    uri,
                    tracknumber,
                    year,
                    timestamp,
                    status,
                    popularity,
                    rate,
                    mtime,
                    ltime,
                    duration,
                    bpm,
                    disc_id, error
            );
            None
        }
        Ok(_) => Some(track_id),
    }
}

/// Add a new album item to database
/// # Parameters
/// * `sql`: Connection to database
/// * `item`: Track item
/// # Returns
/// Track id
pub fn add_item(sql: &dyn Sql, item: &TrackItem) -> Option<String> {
    assert!(sql.writable());

    add(sql, &item.properties)
}

/// Add relation to track
/// # Parameters
/// * `sql`: Connection to database
/// * `track_id`: Target track id
/// * `ids`: Related ids
/// * `relation`: Relation to add
pub fn add_relation(sql: &dyn Sql, track_id: &String, ids: &Vec<String>, relation: Relation) {
    assert!(sql.writable());

    match relation {
        Relation::ArtistIds => {
            for artist_id in ids {
                let query = format!(
                    "REPLACE INTO track_artists (track_id, artist_id)
                                VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![track_id, artist_id]) {
                    Err(error) => error!("Failed to add artist to track: {}", error),
                    Ok(_) => {}
                }
            }
        }
        Relation::GenreIds => {
            for genre_id in ids {
                let query = format!(
                    "REPLACE INTO track_genres (track_id, genre_id)
                                 VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![track_id, genre_id]) {
                    Err(error) => error!("Failed to add genre to track: {}", error),
                    Ok(_) => {}
                }
            }
        }
        Relation::PerformerIds => {
            for performer_id in ids {
                let query = format!(
                    "REPLACE INTO track_performers (track_id, performer_id)
                                 VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![track_id, performer_id]) {
                    Err(error) => error!("Failed to add performer to track: {}", error),
                    Ok(_) => {}
                }
            }
        }
        Relation::ConductorIds => {
            for conductor_id in ids {
                let query = format!(
                    "REPLACE INTO track_conductors (track_id, conductor_id)
                                 VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![track_id, conductor_id]) {
                    Err(error) => error!("Failed to add conductor to track: {}", error),
                    Ok(_) => {}
                }
            }
        }
        Relation::RemixerIds => {
            for remixer_id in ids {
                let query = format!(
                    "REPLACE INTO track_remixers (track_id, remixer_id)
                                 VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![track_id, remixer_id]) {
                    Err(error) => error!("Failed to add remixer to track: {}", error),
                    Ok(_) => {}
                }
            }
        }
        Relation::ComposerIds => {
            for composer_id in ids {
                let query = format!(
                    "REPLACE INTO track_composers (track_id, composer_id)
                                 VALUES (?, ?)"
                );
                match sql.execute(query.as_str(), params![track_id, composer_id]) {
                    Err(error) => error!("Failed to add composer to track: {}", error),
                    Ok(_) => {}
                }
            }
        }
        _ => warn!("Tracks::add_relation() called with invalid Relation"),
    }
}

/// Get mtimes by URI
/// # Parameters
/// * `sql`: Connection to database
/// # Returns
/// A hash containing mtimes
pub fn mtimes_by_uri(sql: &dyn Sql) -> HashMap<String, i64> {
    let mut hash = HashMap::new();
    let mut query = Query {
        select: Select::Sql("tracks.uri, mtime"),
        ..Default::default()
    };
    for row in sql.rows(&query.sql(), params![]) {
        match &row[0] {
            rusqlite::types::Value::Text(uri) => match row[1] {
                rusqlite::types::Value::Integer(mtime) => {
                    hash.insert(String::clone(&uri), mtime);
                }
                _ => error!("Error while reading mtimes..."),
            },
            _ => error!("Error while reading mtimes..."),
        }
    }
    hash
}

/// Get track ids for device
/// # Parameters
/// * `sql`: Connection to database
/// * `device_id`: target device id
/// # Returns
/// Track ids for device
pub fn device_track_ids(sql: &dyn Sql, device_id: i64) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Album),
        where_: Some(Where {
            sql: "synced&(1<<?)",
            ..Default::default()
        }),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![device_id])
}

/// Remove device from database
/// # Parameters
/// * `sql`: Connection to database
/// * `device_id`: target device id
pub fn remove_device(sql: &dyn Sql, device_id: i64) {
    assert!(sql.writable());

    sql.execute(
        "UPDATE tracks SET synced = synced & ~(1<<?)",
        params![device_id],
    )
    .ok();
}

/// Get track ids matching search
/// # Parameters
/// * `sql`: Connection to database
/// * `filters`: Filters to apply
/// * `limit`: Results limit
/// # Returns
/// Track ids
pub fn search(sql: &dyn Sql, filter: &str, limit: i64) -> Vec<String> {
    let mut query = Query {
        select: Select::Object(Object::Track),
        where_: Some(Where {
            sql: "no_accents(title) LIKE %?%",
            ..Default::default()
        }),
        limit: Some(limit),
        ..Default::default()
    };
    sql.strings(&query.sql(), params![filter])
}

/// Get tracks count from database
/// # Parameters
/// * `sql`: Connection to database
/// # Returns
/// Track count
pub fn count(sql: &dyn Sql) -> i64 {
    let mut query = Query {
        select: Select::Sql("COUNT(tracks.*)"),
        ..Default::default()
    };
    sql.integer(&query.sql(), params![])
}

/// Set track popularity
/// # Parameters
/// * `sql`: Connection to database
/// * `track_id`: Target track id
/// * `popularity`: Popularity
pub fn set_popularity(sql: &dyn Sql, track_id: String, popularity: i64) {
    assert!(sql.writable());

    sql.execute(
        "UPDATE tracks set popularity=? WHERE id=?",
        params![popularity, track_id],
    )
    .ok();
}

/// Increase track popularity
/// # Parameters
/// * `sql`: Connection to database
/// * `track_id`: Target track id
pub fn increase_popularity(sql: &dyn Sql, track_id: String) {
    assert!(sql.writable());

    let mut max_query = Query {
        select: Select::Sql("tracks.popularity"),
        orderby: Some(OrderBy::Custom("POPULARITY DESC")),
        limit: Some(1),
        ..Default::default()
    };
    let mut current_query = Query {
        select: Select::Sql("tracks.popularity"),
        where_: Some(Where {
            sql: "tracks.id=?",
            ..Default::default()
        }),
        ..Default::default()
    };
    let max_popularity = sql.integer(&max_query.sql(), params![]);
    let current_popularity = sql.integer(&current_query.sql(), params![track_id]);
    // 10 playbacks should make an track as popular as most popular one
    let increment = (max_popularity - current_popularity) / 10;
    set_popularity(sql, track_id, max(1, increment));
}

/// Clean discs table
/// # Parameters
/// * `sql`: Connection to database
pub fn clean(sql: &dyn Sql) {
    assert!(sql.writable());

    sql.execute(
        "DELETE FROM track_artists
           WHERE track_artists.track_id NOT IN (
            SELECT tracks.id
            FROM tracks)",
        params![],
    )
    .ok();
    sql.execute(
        "DELETE FROM track_genres
           WHERE track_genres.track_id NOT IN (
            SELECT tracks.id
            FROM tracks)",
        params![],
    )
    .ok();
}
