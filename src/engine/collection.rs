// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later
//! Music collection

use super::database::sql::schema::{SCHEMA, UPGRADE};
use super::database::storage_path;

use gio::prelude::*;
use gtk::gio;

use log::debug;
use rusqlite::Connection;
use std::fs::create_dir_all;
use std::path::Path;
use std::path::PathBuf;
use std::sync::LazyLock;
use std::sync::{RwLock, RwLockReadGuard, RwLockWriteGuard};

/// Default music collection
static COLLECTION: LazyLock<RwLock<Collection>> = LazyLock::new(|| RwLock::new(Collection::new()));

/// Music collection.
#[derive(Default)]
pub struct Collection {}

impl<'a> Collection {
    /// Construct a new collection
    #[doc(hidden)]
    pub fn new() -> Self {
        Collection {
            ..Default::default()
        }
    }

    /// Init database
    #[doc(hidden)]
    pub fn init(&self, version: i32) -> bool {
        if !Path::new(storage_path::COLLECTION.as_str()).exists() {
            if let Err(err) = create_dir_all(storage_path::COLLECTION.as_str()) {
                panic!("Can't create directory: {}", err);
            }
        }
        let path = format!("{}/collection.db", storage_path::COLLECTION.as_str());
        let f = gio::File::for_path(&path);
        if !f.query_exists(gio::Cancellable::NONE) {
            match Connection::open(&path) {
                Err(err) => panic!("Can't create database: {}", err),
                Ok(connection) => {
                    for sql in &SCHEMA {
                        debug!("{}", sql);
                        connection.execute(&sql, []).unwrap();
                    }
                    let _ = connection.pragma_update(None, "user_version", version);
                }
            }
            return true;
        }
        false
    }

    /// Upgrade database
    /// Only handle pure SQL requests for now
    #[doc(hidden)]
    pub fn upgrade(&self) {
        let db_path = PathBuf::from(format!(
            "{}/collection.db",
            storage_path::COLLECTION.as_str()
        ));
        match Connection::open(&db_path) {
            Err(err) => panic!("Can't access database: {}", err),
            Ok(db) => {
                let version: i32 = db
                    .query_row("PRAGMA user_version", [], |row| row.get(0))
                    .ok()
                    .unwrap();
                for i in version as usize..UPGRADE.len() {
                    db.execute(&UPGRADE[i], []).unwrap();
                }
            }
        }
    }

    /// Get read access to default collection
    /// # Returns
    /// A read only lock guard on collection
    pub fn read() -> RwLockReadGuard<'a, Collection> {
        COLLECTION.read().unwrap()
    }

    /// Get write access to default collection
    /// # Returns
    /// A read/write lock guard on collection
    pub fn write() -> RwLockWriteGuard<'a, Collection> {
        COLLECTION.write().unwrap()
    }
}
