// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use crate::engine::scanner::channel::Channel;
use crate::engine::scanner::writer::Writer;
use crate::engine::tags::reader::{TagsFile, TagsReader};
use crate::engine::utils::files::{file_type, FileType};
use crate::unwrap_error_or_continue;

use log::{debug, error, warn};
use std::collections::HashMap;
use std::sync::Arc;
use std::sync::RwLock;
use std::time::{SystemTime, UNIX_EPOCH};

pub struct Worker {
    channel: Arc<Channel>,
    mtimes: Arc<RwLock<HashMap<String, i64>>>,
    running: Arc<RwLock<bool>>,
}

impl Worker {
    pub fn new(
        channel: Arc<Channel>,
        mtimes: Arc<RwLock<HashMap<String, i64>>>,
        running: Arc<RwLock<bool>>,
    ) -> Worker {
        Worker {
            channel,
            mtimes,
            running,
        }
    }

    // Scan files for tags
    pub fn scan(&self, files: &HashMap<String, i64>) {
        let empty_db = self.mtimes.read().unwrap().len() != 0;
        let mut tagreader = TagsReader::new();
        let mut tags_files: Vec<TagsFile> = vec![];
        for (uri, uri_mtime) in files.into_iter() {
            if *self.running.read().unwrap() == false {
                break;
            }
            self.channel.send_progress();
            if file_type(&uri) != FileType::AUDIO {
                self.channel.send_progress();
                continue;
            }
            let mut db_mtime: i64 = 0;
            let mut mtime = *uri_mtime;
            match self.mtimes.write().unwrap().remove(uri) {
                Some(value) => {
                    db_mtime = value;
                }
                None => {}
            }
            if mtime > db_mtime {
                // TODO: Remove uri from namespace
                // If not first import, use current time (prevent bad sorting in recents)
                if !empty_db {
                    match SystemTime::now().duration_since(UNIX_EPOCH) {
                        Ok(n) => mtime = n.as_secs() as i64,
                        Err(_) => error!("Could not read current time!"),
                    }
                }
                let file = unwrap_error_or_continue!(tagreader.file_for_uri(uri, mtime));
                tags_files.push(file);
            } else {
                self.channel.send_progress();
            }
        }
        let writer = Writer::new(Arc::clone(&self.channel));
        writer.save(tags_files);
    }
}
