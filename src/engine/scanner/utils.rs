// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::gio;
use gtk::prelude::FileEnumeratorExt;
use gtk::prelude::FileExt;
use log::error;
use std::collections::HashMap;

const SCAN_QUERY_INFO: &str = "standard::name,\
                               standard::type,\
                               standard::is-hidden,\
                               standard::is-symlink,\
                               standard::symlink-target,\
                               time::modified";

/// Split HashMap
pub fn split_hash(hash: HashMap<String, i64>, count: usize) -> Vec<HashMap<String, i64>> {
    let mut split = vec![];
    let mut iterator = hash.iter().peekable();
    let items_per_split = hash.len() / count;
    while let Some(_item) = iterator.peek() {
        let mut split_hash: HashMap<String, i64> = HashMap::new();
        while let Some(item) = iterator.next() {
            split_hash.insert(String::from(item.0), *item.1);
            if split_hash.len() > items_per_split {
                break;
            }
        }
        split.push(split_hash);
    }
    split
}

/// Get files, directories for URIS
///
/// # Parameters
///
/// * `uris`: target URIS
/// * `follow_symlinks`: should we follow symlinks?
/// # Returns
/// * Music files vector
/// * Directory URIs
/// * Empty Uris
pub fn get_uris_content(
    uris: &Vec<String>,
    follow_symlinks: bool,
) -> (HashMap<String, i64>, Vec<String>, Vec<String>) {
    let mut files = HashMap::new();
    let mut dirs = vec![];
    let mut empty_uris = vec![];
    let mut walk_uris = uris.to_vec();
    while walk_uris.len() != 0 {
        let uri = walk_uris.pop().unwrap();
        let file = gio::File::for_uri(&uri);
        match file.query_info(
            SCAN_QUERY_INFO,
            gio::FileQueryInfoFlags::NONE,
            gio::Cancellable::NONE,
        ) {
            Ok(info) => {
                match info.file_type() {
                    gio::FileType::Directory => {
                        let mut is_empty = true;
                        dirs.push(uri.clone());
                        match file.enumerate_children(
                            SCAN_QUERY_INFO,
                            gio::FileQueryInfoFlags::NONE,
                            gio::Cancellable::NONE,
                        ) {
                            Ok(children) => {
                                for child in children.clone() {
                                    is_empty = false;
                                    match child {
                                        Ok(info) => {
                                            // Ignore hidden files and symlinks
                                            if info.is_hidden()
                                                || (info.is_symlink() && !follow_symlinks)
                                            {
                                                continue;
                                            }
                                            let child = children.child(&info);
                                            let uri = child.uri().to_string();
                                            walk_uris.push(uri);
                                        }
                                        Err(error) => {
                                            error!(
                                                "plugin::collection::main::get_uris_content: {}",
                                                error
                                            )
                                        }
                                    }
                                }
                                children.close(gio::Cancellable::NONE).ok();
                            }
                            Err(error) => {
                                error!("plugin::collection::main::get_uris_content: {}", error)
                            }
                        }
                        if is_empty && uris.contains(&uri) {
                            empty_uris.push(uri);
                        }
                    }
                    gio::FileType::Regular => {
                        let mut mtime = 0;
                        match info.attribute_as_string("time::modified") {
                            Some(value) => {
                                mtime = value.parse().unwrap();
                            }
                            None => {}
                        }
                        files.insert(uri, mtime);
                    }
                    gio::FileType::SymbolicLink => {
                        if follow_symlinks {
                            let mut mtime = 0;
                            match info.attribute_as_string("time::modified") {
                                Some(value) => mtime = value.parse().unwrap(),
                                None => {}
                            }
                            files.insert(uri, mtime);
                        }
                    }
                    _ => {}
                }
            }
            Err(error) => {
                error!("plugin::collection::main::get_uris_content: {}", error)
            }
        }
    }
    (files, dirs, empty_uris)
}
