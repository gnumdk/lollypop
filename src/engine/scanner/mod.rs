// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

pub mod channel;
pub mod utils;
pub mod worker;
pub mod writer;

use crate::engine::bus::{send_message, Message as AppMessage};
use crate::engine::database::methods::{albums, artists, discs, genres, tracks};
use crate::engine::database::sql::connection::{Connection, Transaction};
use crate::engine::database::sql::utils::connection;
use channel::Channel;
use worker::Worker;

use async_channel::Sender;
use log::warn;
use std::sync::Arc;
use std::sync::RwLock;
use std::thread;

#[derive(Debug)]
pub enum Message {
    ScannerStarted,
    ScannerFinished,
    ScannerProgress(usize, usize), // current, total
}

#[derive(Default)]
pub struct Scanner {
    running: Arc<RwLock<bool>>,
}

impl Scanner {
    // Scan repositories for music
    ///
    /// # Parameters
    ///
    /// * `uris`: target URIS
    /// * `sender`: async channel for progress status
    pub fn scan(&self, uris: Vec<String>, sender: Arc<RwLock<Option<Sender<AppMessage>>>>) {
        let (files, _dirs, empty_uris) = utils::get_uris_content(&uris, false);
        let db_mtimes;

        if empty_uris.len() > 0 {
            warn!("scan: Empty collection detected!");
            return;
        }

        {
            let connection = Connection::ro(connection());
            db_mtimes = Arc::new(RwLock::new(tracks::mtimes_by_uri(&connection)));
        }

        let channel = Arc::new(Channel::new(sender, files.len() * 2));
        *self.running.write().unwrap() = true;
        //Min: 1 thread, Max: 5 threads
        let num_threads = std::cmp::max(1, num_cpus::get() / 2);
        let split_files = utils::split_hash(files, num_threads);
        let mut thread_handles = vec![];
        send_message(AppMessage::Scanner(Message::ScannerStarted));
        for files in split_files {
            let worker = Worker::new(
                Arc::clone(&channel),
                Arc::clone(&db_mtimes),
                Arc::clone(&self.running),
            );
            let handle = thread::spawn(move || worker.scan(&files));
            thread_handles.push(handle);
        }

        //Wait all threads to finish
        while thread_handles.len() != 0 {
            let handle = thread_handles.pop().unwrap();
            handle.join().unwrap();
        }
        self.clean();
        send_message(AppMessage::Scanner(Message::ScannerFinished));
    }

    fn clean(&self) {
        let mut connection = connection();
        let transaction = Transaction::new(connection.transaction().unwrap());

        tracks::clean(&transaction);
        albums::clean(&transaction);
        discs::clean(&transaction);
        artists::clean(&transaction);
        genres::clean(&transaction);
    }
}
