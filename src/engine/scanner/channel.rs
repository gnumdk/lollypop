// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use super::super::bus::{send_message, Message as AppMessage};
use super::Message::ScannerProgress;

use async_channel::Sender;
use std::sync::{Arc, RwLock};

pub struct Channel {
    sender: Arc<RwLock<Option<Sender<AppMessage>>>>,
    done: RwLock<usize>,
    total: RwLock<usize>,
}

impl Channel {
    pub fn new(sender: Arc<RwLock<Option<Sender<AppMessage>>>>, total: usize) -> Channel {
        Channel {
            sender,
            total: RwLock::new(total),
            done: RwLock::new(0),
        }
    }

    pub fn send_progress(&self) {
        let total = self.total.read().unwrap();
        let mut done = self.done.write().unwrap();

        *done += 1;

        send_message(AppMessage::Scanner(ScannerProgress(*done, *total)));
    }
}
