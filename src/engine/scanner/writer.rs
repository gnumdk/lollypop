// Copyright (c) 2021-2022 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

use crate::engine::collection::Collection;
use crate::engine::database::methods::{albums, artists, discs, genres, tracks};
use crate::engine::database::objects::properties::Property;
use crate::engine::database::objects::relations::Relation;
use crate::engine::database::sql::connection::Transaction;
use crate::engine::database::sql::utils::connection;
use crate::engine::scanner::channel::Channel;
use crate::engine::tags::item::{DiscItem, Item, TrackItem};
use crate::engine::tags::reader::{TagKey, TagValue, TagsFile};
use crate::{cast_enum, split_tags_semicolon, unwrap_or_continue};

use log::warn;
use std::collections::HashMap;
use std::sync::{Arc, RwLock};

macro_rules! add_item_to_db {
    ($transaction: expr, $items: expr, $method: path) => {{
        let mut ids = vec![];
        for item in $items {
            let id = unwrap_or_continue!($method($transaction, &item));
            ids.push(id);
        }
        ids
    }};
}

macro_rules! create_items {
    ($items: expr) => {{
        let mut items = vec![];
        for item in $items {
            let mut properties: HashMap<Property, rusqlite::types::Value> = HashMap::new();
            properties.insert(Property::Name, rusqlite::types::Value::Text(item.clone()));
            properties.insert(Property::Sortname, rusqlite::types::Value::Text(item));
            items.push(Item { properties });
        }
        items
    }};
    ($items: expr, $mbids: expr, $sortnames: expr) => {{
        let mut artist_items = vec![];
        for item in $items {
            let mut properties: HashMap<Property, rusqlite::types::Value> = HashMap::new();
            let sortname: String;
            // Sortnames may be shorter than artists
            if $sortnames.len() > 0 {
                sortname = $sortnames.remove(0);
            } else {
                sortname = item.clone();
            }
            properties.insert(Property::Name, rusqlite::types::Value::Text(item));
            properties.insert(Property::Sortname, rusqlite::types::Value::Text(sortname));
            if $mbids.len() > 0 {
                let mbid = $mbids.remove(0);
                properties.insert(Property::Mbid, rusqlite::types::Value::Text(mbid));
            }
            artist_items.push(Item { properties });
        }
        artist_items
    }};
}

pub struct Writer {
    channel: Arc<Channel>,
}

impl Writer {
    pub fn new(channel: Arc<Channel>) -> Writer {
        Writer { channel }
    }

    // Save tags to collection
    pub fn save(&self, tags_list: Vec<TagsFile>) {
        let mut connection = connection();
        let transaction = Transaction::new(connection.transaction().unwrap());
        for mut tags in tags_list {
            let album_item = self.get_album_item(&mut tags);
            let disc_item = self.get_disc_item(&mut tags, album_item);
            let mut track_item = self.get_track_item(&mut tags, disc_item);
            let genre_ids = add_item_to_db!(
                &transaction,
                &track_item.disc_item.genre_items,
                genres::add_item
            );
            let artist_ids =
                add_item_to_db!(&transaction, &track_item.artist_items, artists::add_item);
            let composer_ids =
                add_item_to_db!(&transaction, &track_item.composer_items, artists::add_item);
            let conductor_ids =
                add_item_to_db!(&transaction, &track_item.conductor_items, artists::add_item);
            let remixer_ids =
                add_item_to_db!(&transaction, &track_item.remixer_items, artists::add_item);
            let performer_ids =
                add_item_to_db!(&transaction, &track_item.performer_items, artists::add_item);
            let disc_artist_ids = add_item_to_db!(
                &transaction,
                &track_item.disc_item.artist_items,
                artists::add_item
            );
            let album_id = unwrap_or_continue!(albums::add_item(
                &transaction,
                &track_item.disc_item.album_item,
                &disc_artist_ids,
            ));
            track_item
                .disc_item
                .properties
                .insert(Property::AlbumId, rusqlite::types::Value::Text(album_id));
            let disc_id = unwrap_or_continue!(discs::add_item(&transaction, &track_item.disc_item));
            track_item.properties.insert(
                Property::DiscId,
                rusqlite::types::Value::Text(disc_id.clone()),
            );
            let track_id = unwrap_or_continue!(tracks::add_item(&transaction, &track_item));
            discs::add_relation(
                &transaction,
                &disc_id,
                &disc_artist_ids,
                Relation::ArtistIds,
            );
            discs::add_relation(&transaction, &disc_id, &genre_ids, Relation::GenreIds);
            tracks::add_relation(&transaction, &track_id, &artist_ids, Relation::ArtistIds);
            tracks::add_relation(&transaction, &track_id, &remixer_ids, Relation::RemixerIds);
            tracks::add_relation(
                &transaction,
                &track_id,
                &composer_ids,
                Relation::ComposerIds,
            );
            tracks::add_relation(
                &transaction,
                &track_id,
                &performer_ids,
                Relation::PerformerIds,
            );
            tracks::add_relation(
                &transaction,
                &track_id,
                &conductor_ids,
                Relation::ConductorIds,
            );
            tracks::add_relation(&transaction, &track_id, &genre_ids, Relation::GenreIds);
            self.channel.send_progress();
        }
    }

    fn get_album_item(&self, file: &TagsFile) -> Item {
        let title = cast_enum!(file.get(&TagKey::AlbumTitle), TagValue::AlbumTitle);
        let mbid = cast_enum!(file.get(&TagKey::AlbumMbid), TagValue::AlbumMbid);
        let is_compilation = cast_enum!(file.get(&TagKey::IsCompilation), TagValue::IsCompilation);
        let date = cast_enum!(file.get(&TagKey::Date), TagValue::Date);
        let mut properties: HashMap<Property, rusqlite::types::Value> = HashMap::new();
        properties.insert(Property::Title, rusqlite::types::Value::Text(title));
        if let Some(value) = mbid {
            properties.insert(Property::Mbid, rusqlite::types::Value::Text(value));
        }
        properties.insert(
            Property::IsCompilation,
            rusqlite::types::Value::Integer(i64::from(is_compilation)),
        );
        if let Some((year, timestamp)) = date {
            properties.insert(Property::Year, rusqlite::types::Value::Integer(year));
            properties.insert(
                Property::Timestamp,
                rusqlite::types::Value::Integer(timestamp),
            );
        }
        let filename = file.uri.split("/").collect::<Vec<&str>>().pop().unwrap();
        properties.insert(
            Property::Uri,
            rusqlite::types::Value::Text(file.uri.replace(filename, "")),
        );
        Item { properties }
    }

    fn get_disc_item(&self, file: &TagsFile, album_item: Item) -> DiscItem {
        let genres = split_tags_semicolon!(cast_enum!(file.get(&TagKey::Genres), TagValue::Genres));
        let artists = split_tags_semicolon!(cast_enum!(
            file.get(&TagKey::AlbumArtists),
            TagValue::AlbumArtists
        ));
        let mut artist_sortnames = split_tags_semicolon!(cast_enum!(
            file.get(&TagKey::AlbumArtistSortnames),
            TagValue::AlbumArtistSortnames
        ));
        let mut artist_mbids = split_tags_semicolon!(cast_enum!(
            file.get(&TagKey::AlbumArtistMbids),
            TagValue::AlbumArtistMbids
        ));
        let title = cast_enum!(file.get(&TagKey::DiscTitle), TagValue::DiscTitle);
        let number = cast_enum!(file.get(&TagKey::DiscNumber), TagValue::DiscNumber);
        let mut properties: HashMap<Property, rusqlite::types::Value> = HashMap::new();
        if let Some(value) = title {
            properties.insert(Property::Title, rusqlite::types::Value::Text(value));
        }
        if let Some(value) = number {
            properties.insert(Property::Number, rusqlite::types::Value::Integer(value));
        }
        let artist_items = create_items!(artists, artist_mbids, artist_sortnames);
        let genre_items = create_items!(genres);
        DiscItem {
            properties,
            artist_items,
            album_item,
            genre_items,
        }
    }

    fn get_track_item(&self, file: &TagsFile, disc_item: DiscItem) -> TrackItem {
        let artists =
            split_tags_semicolon!(cast_enum!(file.get(&TagKey::Artists), TagValue::Artists));
        let mut artist_sortnames = split_tags_semicolon!(cast_enum!(
            file.get(&TagKey::ArtistSortnames),
            TagValue::ArtistSortnames
        ));
        let mut artist_mbids = split_tags_semicolon!(cast_enum!(
            file.get(&TagKey::ArtistMbids),
            TagValue::ArtistMbids
        ));
        let composers = split_tags_semicolon!(cast_enum!(
            file.get(&TagKey::Composers),
            TagValue::Composers
        ));
        let performers = split_tags_semicolon!(cast_enum!(
            file.get(&TagKey::Performers),
            TagValue::Performers
        ));
        let remixers =
            split_tags_semicolon!(cast_enum!(file.get(&TagKey::Remixers), TagValue::Remixers));
        let conductors = split_tags_semicolon!(cast_enum!(
            file.get(&TagKey::Conductors),
            TagValue::Conductors
        ));
        let title = cast_enum!(file.get(&TagKey::Title), TagValue::Title);
        let mbid = cast_enum!(file.get(&TagKey::TrackMbid), TagValue::TrackMbid);
        let original_date = cast_enum!(file.get(&TagKey::OriginalDate), TagValue::OriginalDate);
        let rate = cast_enum!(file.get(&TagKey::Rate), TagValue::Rate);
        let bpm = cast_enum!(file.get(&TagKey::Bpm), TagValue::Bpm);
        let number = cast_enum!(file.get(&TagKey::TrackNumber), TagValue::TrackNumber);
        let duration = cast_enum!(file.get(&TagKey::Duration), TagValue::Duration);
        let mut properties: HashMap<Property, rusqlite::types::Value> = HashMap::new();
        properties.insert(Property::Title, rusqlite::types::Value::Text(title));
        if let Some(value) = mbid {
            properties.insert(Property::Mbid, rusqlite::types::Value::Text(value));
        }
        properties.insert(
            Property::Uri,
            rusqlite::types::Value::Text(String::from(file.uri)),
        );
        if let Some((year, timestamp)) = original_date {
            properties.insert(Property::Year, rusqlite::types::Value::Integer(year));
            properties.insert(
                Property::Timestamp,
                rusqlite::types::Value::Integer(timestamp),
            );
        }
        properties.insert(Property::Rate, rusqlite::types::Value::Integer(rate));
        if let Some(value) = bpm {
            properties.insert(Property::Bpm, rusqlite::types::Value::Integer(value));
        }
        if let Some(value) = number {
            properties.insert(Property::Number, rusqlite::types::Value::Integer(value));
        }
        properties.insert(
            Property::Duration,
            rusqlite::types::Value::Integer(duration),
        );
        properties.insert(Property::Mtime, rusqlite::types::Value::Integer(file.mtime));

        let artist_items = create_items!(artists, artist_mbids, artist_sortnames);
        let composer_items = create_items!(composers);
        let performer_items = create_items!(performers);
        let remixer_items = create_items!(remixers);
        let conductor_items = create_items!(conductors);
        TrackItem {
            properties,
            disc_item,
            artist_items,
            composer_items,
            performer_items,
            remixer_items,
            conductor_items,
        }
    }
}
