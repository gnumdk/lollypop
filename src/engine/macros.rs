// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Various useful macros

/// Cast an enum to the inner type
#[macro_export]
macro_rules! cast_enum {
    ($target_enum: expr, $target_path: path) => {
        if let $target_path(value) = $target_enum {
            value
        } else {
            panic!("cast_enum called for an invalid path");
        }
    };
}
