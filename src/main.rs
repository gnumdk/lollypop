// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later
// #![cfg_attr(feature = "dox", feature(doc_cfg))]
// #![allow(clippy::needless_doctest_main)]
// #![doc(
//     html_logo_url = "https://gitlab.gnome.org/World/lollypop/-/raw/master/data/icons/hicolor/512x512/apps/org.gnome.Lollypop.png",
//     html_favicon_url = "https://gitlab.gnome.org/GNOME/libadwaita/-/raw/main/demo/data/org.gnome.Adwaita1.Demo-symbolic.svg"
// )]

mod application;
mod config;
mod engine;
mod models;
mod widgets;

use crate::application::Application;

use config::{APPLICATION_ID, GETTEXT_PACKAGE, LOCALEDIR, PKGDATADIR};
use gettextrs::{bind_textdomain_codeset, bindtextdomain, textdomain};
use gtk::prelude::*;
use gtk::{gio, glib};

use std::env;
extern crate log;

fn main() -> glib::ExitCode {
    // Initialize logger
    env::set_var("RUST_LOG", "INFO");
    env_logger::init();

    // Set up gettext translations
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8")
        .expect("Unable to set the text domain encoding");
    textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    // Load resources
    let resources =
        gio::Resource::load(PKGDATADIR.to_owned() + "/" + APPLICATION_ID + ".gresource")
            .expect("Could not load resources");
    gio::resources_register(&resources);
    gstreamer::init().expect("Unable to start GStreamer");

    let app = Application::new(APPLICATION_ID, &gio::ApplicationFlags::empty());
    app.run()
}
