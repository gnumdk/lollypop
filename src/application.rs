// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

pub mod window;

use adw::subclass::prelude::*;
use glib::{clone, WeakRef};
use gtk::prelude::*;
use gtk::{gdk, gio, glib};

use crate::application::window::ApplicationWindow;
use crate::config::VERSION;
use crate::engine::{
    bus::{BusMessageImpl, Message},
    collection::Collection,
};

use async_channel::{Receiver, Sender};
use log::{debug, warn};
use std::cell::{OnceCell, RefCell};
use std::sync::{Arc, RwLock};

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct Application {
        pub window: OnceCell<WeakRef<ApplicationWindow>>,

        pub sender: Arc<RwLock<Option<Sender<Message>>>>,
        pub receiver: RefCell<Option<Receiver<Message>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "Lollypop";
        type Type = super::Application;
        type ParentType = adw::Application;

        fn new() -> Self {
            let (s, r) = async_channel::unbounded();
            let sender = Arc::new(RwLock::new(Some(s)));
            let receiver = RefCell::new(Some(r));

            Self {
                window: OnceCell::new(),
                sender,
                receiver,
            }
        }
    }

    impl ObjectImpl for Application {
        // Setup accelerators
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.setup_gactions();
            obj.setup_channel();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
        }
    }

    impl ApplicationImpl for Application {
        fn activate(&self) {
            let application = self.obj();
            let window = ApplicationWindow::new(&*application);

            self.window
                .set(window.downgrade())
                .expect("Window already set.");

            window.present();
        }

        fn startup(&self) {
            let application = self.obj();

            // This call gtk_init()
            self.parent_startup();

            application.setup_css();

            let collection = Collection::write();
            if !collection.init(0) {
                collection.upgrade();
            }
        }
    }

    impl GtkApplicationImpl for Application {}
    impl AdwApplicationImpl for Application {}
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl Application {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::builder()
            .property("application-id", application_id)
            .property("flags", flags)
            .build()
    }

    fn setup_gactions(&self) {
        let quit_action = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| app.quit())
            .build();
        let about_action = gio::ActionEntry::builder("about")
            .activate(move |app: &Self, _, _| app.show_about())
            .build();
        self.add_action_entries([quit_action, about_action]);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let about = adw::AboutWindow::builder()
            .transient_for(&window)
            .application_name("Lollypop")
            .application_icon("org.adishatz.Lollypop")
            .developer_name("Cédric Bellegarde")
            .version(VERSION)
            .developers(vec!["Cédric Bellegarde"])
            .copyright("© 2024 Cédric Bellegarde")
            .build();

        about.present();
    }

    fn setup_css(&self) {
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/style.css");
        if let Some(display) = gdk::Display::default() {
            gtk::style_context_add_provider_for_display(
                &display,
                &provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }
    }

    fn setup_channel(&self) {
        let receiver = self.imp().receiver.borrow_mut().take().unwrap();
        glib::MainContext::default().spawn_local(clone!(
            #[strong(rename_to = this)]
            self,
            async move {
                use futures::prelude::*;

                let mut receiver = std::pin::pin!(receiver);

                while let Some(message) = receiver.next().await {
                    debug!("Bus message: {:?}", message);
                    this.handle(&message);
                }
            }
        ));
    }

    pub fn sender(&self) -> Arc<RwLock<Option<Sender<Message>>>> {
        Arc::clone(&self.imp().sender)
    }
}

impl BusMessageImpl for Application {
    fn handle(&self, message: &Message) {
        let this = self.imp();
        if let Some(window) = this.window.get() {
            let window = window.upgrade().unwrap();
            window.handle(message);
        }
    }
}
