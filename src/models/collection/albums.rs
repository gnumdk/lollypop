// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use async_channel::Sender;
use std::cell::RefCell;

use glib::{clone, WeakRef};
use gtk::glib;

use super::model::Model;
use super::ModelType;
use crate::engine::database::{
    filters::Filters,
    methods::albums,
    sql::{builder::OrderBy, connection::Connection, utils::connection},
    StatusFlags,
};

pub struct Albums {
    model: Model,
    filters: Option<Filters>,
}

impl Albums {
    pub fn new() -> Albums {
        let model = Model::new(ModelType::Album);
        Albums {
            model,
            filters: None,
        }
    }

    pub fn set_filters(&mut self, filters: Option<Filters>) {
        self.filters = filters;
        self.update_filtering();
    }

    pub fn ids(&self) -> Vec<String> {
        let connection = Connection::ro(connection());

        albums::ids(
            &connection,
            self.filters.clone(),
            Some(OrderBy::ArtistYear),
            false,
            StatusFlags::ALL,
            None,
        )
    }

    fn update_filtering(&self) {
        let (s, r) = async_channel::unbounded::<Vec<String>>();
        glib::MainContext::default().spawn_local(clone!(
            #[strong(rename_to = this)]
            self,
            async move {
                use futures::prelude::*;

                let mut receiver = std::pin::pin!(r);

                if let Some(message) = receiver.next().await {
                    log::warn!("Bus message: {:?}", message);
                }
            }
        ));
        let ids = self.ids();
        s.send_blocking(ids).ok();
    }
}
