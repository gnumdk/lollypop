// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use std::cell::{Cell, RefCell};

mod imp {
    use super::*;
    use glib::{ParamSpec, Properties, Value};

    #[derive(Default, Properties)]
    #[properties(wrapper_type = super::Item)]
    pub struct Item {
        #[property(get, set)]
        pub id: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Item {
        const NAME: &'static str = "Item";
        type ParentType = glib::Object;
        type Type = super::Item;
    }

    impl ObjectImpl for Item {
        fn properties() -> &'static [ParamSpec] {
            Self::derived_properties()
        }
        fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }
        fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
            self.derived_property(id, pspec)
        }
    }
}

glib::wrapper! {
    pub struct Item(ObjectSubclass<imp::Item>);
}

impl Item {
    pub fn new(id: String) -> Self {
        let item = glib::Object::builder::<Self>().build();
        let this = item.imp();
        this.id.replace(id.clone());
        item
    }
}
