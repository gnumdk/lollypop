pub mod albums;
pub mod item;
pub mod model;

use gtk::glib;

#[derive(glib::Enum, Debug, Copy, Clone, PartialEq, Eq, Default)]
#[enum_type(name = "ModelType")]
pub enum ModelType {
    #[default]
    Genre,
    Artist,
    Album,
    Track,
    Playlist,
}
