// SPDX-FileCopyrightText: Cédric Bellegarde <cedric.bellegarde@adishatz.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use std::cell::OnceCell;
use std::cell::RefCell;
use std::rc::Rc;

use super::item::Item;
use super::ModelType;

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct Model {
        pub items: RefCell<Vec<Item>>,
        pub model_type: OnceCell<ModelType>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Model {
        const NAME: &'static str = "Model";
        type Type = super::Model;
        type ParentType = glib::Object;
        type Interfaces = (gio::ListModel,);
    }

    impl ObjectImpl for Model {}

    impl ListModelImpl for Model {
        fn item_type(&self) -> glib::Type {
            Item::static_type()
        }
        fn n_items(&self) -> u32 {
            self.items.borrow().len() as u32
        }
        fn item(&self, position: u32) -> Option<glib::Object> {
            self.items
                .borrow()
                .get(position as usize)
                .map(|o| o.clone().upcast::<glib::Object>())
        }
    }
}

glib::wrapper! {
    pub struct Model(ObjectSubclass<imp::Model>)
        @implements gio::ListModel;
}

impl Model {
    pub fn new(model_type: ModelType) -> Model {
        let model: Model = glib::Object::builder().build();
        let this = model.imp();
        this.model_type.set(model_type).ok();
        model
    }
}
